# Copyright (c) 2019, Alliance for Open Media. All rights reserved
#
# This source code is subject to the terms of the BSD 2 Clause License and
# the Alliance for Open Media Patent License 1.0. If the BSD 2 Clause License
# was not distributed with this source code in the LICENSE file, you can
# obtain it at www.aomedia.org/license/software. If the Alliance for Open
# Media Patent License 1.0 was not distributed with this source code in the
# PATENTS file, you can obtain it at www.aomedia.org/license/patent.

'''
***Parameter Setups***

SPIE2020:
Resolutions = [(1280,720),(960,540), (768,432), (608,342), (480,270), (384,216)]    10bit
Resolutions = [(1280,720),(960,540),(640,360),(480,270)]                            8bit

9QPs
QP_VALUES   = [14,18,22,27,32,37,42,47,51] # x264,x265
QP_VALUES   = [20,26,32,37,43,48,55,59,63] # svt, aom


SPIE2021:
Resolution = [(1280,720),(960,540),(768,432),(640,360),(512,288),(384,216),(256,144)] 8bit

11QPs
QP_VALUES   = [19,21,23,25,27,29,31,33,35,37,41] # x264, x265
QP_VALUES   = [23,27,31,35,39,43,47,51,55,59,63] # svt, aom



BOTH:

passes                   = -1
cqp                      = 0
intra_period             = -1
lp_number                = 1
live_encoding            = 0
rate_control             = 0

scene_cut_clip           = 0
downscale_converter      = 1

downscale_algo           = "lanczos"
ffmpeg_rescale_abr       = 0

downscale_commands       = 1
rescale_bdrate           = 1
metrics_computation      = 2

psnr_ssim_generation     = 0
vmaf_generation          = 0

disable_enhancement_vmaf = 0

#####/// x265 parameters ///#####
pools                       = -1
no_scenecut                 = 1

encoder_cfg_list = []
'''

from copy import deepcopy
import os
import stat
import re
import ast
import sys
import multiprocessing
# def generate_tests(downscale_commands, rescale_bdrate, metrics_computation, ENCODER, psnr_ssim_generation, vmaf_generation):

###################################### =====Configuration Start ===== ######################################
#Location of the clips to be encoded
CLIP_DIRS                = [r'/home/user/stream']     ### clips to test


ENC_MODES                = [12]                              ### presets to test
passes                   = -1                               ### 1-pass or 2-pass encoding, not supported by all encoders
cqp                      = 0                                ### enable/disable CQP encodings
intra_period             = -1
lp_number                = 1                                ### [-1/x]: -1 = omitted | x = -lp x
live_encoding            = 0                                ### 1:Change encoding command lines to live encoding command lines | 0: VOD command lines ### Overrides num_pool as well
rate_control             = 0                                ### 0:CRF | 1:VBR | 2:CBR
crf_based_target_bitrate = 0                                ### Select target bitrate per corresponding CRF encode

if crf_based_target_bitrate:
    # Directory containing CRF encodings; Set QP values to correspond to the CRF
    # Encodings you want to generate corresponding commands for
    CRF_DIR                  = [r'/home/ubuntu/CRF/bitstreams']


add_commands = '' #add commands to be inserted into commands line
if live_encoding:
    lp_number = -1


#Enable/disable cutting clips based on text file
scene_cut_clip                 = 0
#Suffix of files containing a new line separated list of frame boundaries
#Place in same folder as clips
#i.e: clip1.y4m, clip1_scenes.txt
scene_changes_suffix     = "_scenes.txt"


'''scaling options'''
downscale_converter           = 1        ### [0/1]: 0 = HDRtools | 1 = ffmpeg


# downscale_target_resolutions           = [("width1", "height1"),("width2", "height2")]
# downscale_target_resolutions           = [(1280,720),(960,540), (768,432), (608,342), (480,270), (384,216)] #10bit
# downscale_target_resolutions           = [(1280,720),(960,540),(640,360),(480,270)] #8bit
downscale_target_resolutions             = [(1280,720),(960,540),(768,432),(640,360),(512,288),(384,216),(256,144)] #8bit elfuente
''''''''''''''''''''''''''''''''''''''''''''''''


#Downscaling algorithm to use. please see https://ffmpeg.org/ffmpeg-scaler.html for
#all options
downscale_algo            = "lanczos"
#downscale_algo           = "bilinear"
#downscale_algo           = "bicubic"
#downscale_algo           = "gauss"
#downscale_algo           = "sinc"


ffmpeg_rescale_abr = 0
ffmpeg_rescale_abr_target_resolutions=[['1920x1080','5000'],['1280x720','2500'],['960x540','1250'],['640x360','625'],['288x160','312']]


#Generates PSNR/SSIM/VMAF commands for rescale pipeline
downscale_commands  = 1         ### [0/1]: 0 = disable downscaling clips | 1 = enable downscaling clips
metrics_computation = 0         ### [0/1]: 0 = libvmaf | 1 = ffmpeg | 2 = Decode with ffmpeg and extract with vmaf executable

'''metric commands generation'''
rescale_bdrate               = 1         ### Generates PSNR/SSIM/VMAF commands for rescale pipeline

#!!! NOTE: metrics_computation = 1 options only (ffmpeg metrics) | If rescale_bdrate = 1, the following options should = 0 |

psnr_ssim_generation         = 0     ### Enable/disable ffmpeg command generation for non-rescale pipeline
vmaf_generation              = 0     ### Enable/disable vmaf command generation for non-rescale pipeline

enable_vmaf_threading = 0       ### 0 = Disabled | 1 = Enabled (Metrics Computation = 2)
force_number_of_threads = 0     ### [0/x]: 0 = Default #cores/#jobs | x = number_of_threads

#####################################################################


disable_enhancement_vmaf    = 0     ### Enable/disable usage of vmaf disable enhancement model 0=VMAF 1=VMAF-NEG
# delete_intermediary       = 0     ### Delete intermediary YUV/Y4M files after collecting data


num_pool                 = 96                               ### number of encoding jobs to run in parallel
if live_encoding == 1:
    num_pool = 1                                            ### Override num_pool

ffmpeg_job_count         = 20                               ### number of ffmpeg jobs to run in parallel
vmaf_job_count           = 20                               ### number of vmaf jobs to run in parallel (metrics_computation = 2)

bitstream_folders        = ['bitstreams/']


ENCODER                  = "svt"
#ENCODER                  = "libaom"
#ENCODER                  = "x264"
#ENCODER                  = "x265"
#ENCODER                  = "vp9"
##ENCODER                  = "ffmpeg-svt"
#ENCODER                  = "ffmpeg-libvpx"
#ENCODER                  = "ffmpeg-libaom"
#ENCODER                  = "ffmpeg-x264"
#ENCODER                  = "ffmpeg-x265"
#ENCODER                  = "vvenc"

number_of_cores = multiprocessing.cpu_count()
number_of_threads = number_of_cores/vmaf_job_count

if force_number_of_threads != 0:
    number_of_threads = force_number_of_threads

if passes == -1:###Feature 7
    if ENCODER in ['libaom','vp9','ffmpeg-libaom','ffmpeg-libvpx']:
        passes=2
    else:
        passes=1

if ENCODER in ['x264','x265', "vvenc", 'ffmpeg-x264','ffmpeg-x265']:
    #QP_VALUES               = [22,27,32,37,47]
    TBR_VALUES              = [5000,4000,3000,2000]
    # QP_VALUES              = [14, 18, 22, 27 ,32, 37, 42, 47, 51]
    QP_VALUES               = [19,21,23,25,27,29,31,33,35,37,41] #elfuente_qps

else:
    #QP_VALUES               = [20,32,43,55,63]
    TBR_VALUES              = [5000,4000,3000,2000]
    # QP_VALUES               = [20,26,32,37,43,48,55,59,63]
    QP_VALUES               = [23,27,31,35,39,43,47,51,55,59,63] #elfuente_qps

#####/// x265 parameters ///#####
pools                       = -1    ### [-1/x]: -1 = omitted, x = --pools x
no_scenecut                 = 1    ### [-1/1]: -1 = omitted, 1 = --no-scenecut
#####//////#####

if crf_based_target_bitrate:
    RC_VALUES = QP_VALUES
elif rate_control in [1,2]:
    RC_VALUES = TBR_VALUES
else:
    RC_VALUES = QP_VALUES

encoder_cfg_list = []
if ENCODER == "libaom" or ENCODER == "vp9":
    encoder_cfg_list.append({'enc_name': 'aom','config_token': ' -c ','enc_mode_token': '--cpu-used=','fps_token': '--fps=',
                             'input_token': '  ','width_token': '--width=','height_token': '--height=','qp_value_token': '--cq-level=',
                             'fps_num': '-fps-num','fps_denom': '-fps-denom','intra_period_token': '--kf-min-dist=',
                             'intra_period_token_max': '--kf-max-dist=','bitdepth_token': '--bit-depth=','inbitdepth_token': '--input-bit-depth=',
                             'bitstream_token': ' -o ','num_frames_token': ' -n ','rc_token': ' -rc ','tbr_token': ' -tbr ','mbr_token': ' -mbr ',
                             'mbs_token': ' -mbs ','out_1pass': ' -use-output-stat-file ','in_2pass': ' -use-input-stat-file ',
                             'lad_token' : '-lad', 'lp_token': '-lp', 'passes_token': ' --passes=', 'qp_mode_token': '--end-usage=q ',
                             'auto_alt_ref_token': '--auto-alt-ref=', 'lag_in_frames_token': '--lag-in-frames=',
                             'use_fixed_qp_offsets_token': '--use-fixed-qp-offsets=', 'enable_tpl_model_token': '--enable-tpl-model=',
                             'min_gf_interval_token': '--min-gf-interval=', 'max_gf_interval_token': '--max-gf-interval=',
                             'gf_min_pyr_height_token': '--gf-min-pyr-height=', 'gf_max_pyr_height_token': '--gf-max-pyr-height=',
                             'aq_mode_token': '--aq-mode=', 'deltaq_token': '--deltaq-mode=', 'threads_token': ' --threads='})

elif ENCODER == "svt":
    encoder_cfg_list.append({'enc_name': 'svt-av1','config_token': ' -c ','enc_mode_token': '--preset','input_token': ' -i ',
                             'width_token': '-w','height_token': '-h','qp_value_token': '-q','fps_num': '--fps-num',
                             'fps_denom': '--fps-denom','intra_period_token': ' --keyint ','bitdepth_token': '--input-depth',
                             'bitstream_token': ' -b ','num_frames_token': ' -n ','rc_token': ' --rc ','tbr_token': ' --tbr ',
                             'mbr_token': ' --mbr ', 'lad_token' : '--lookahead', 'lp_token': '--lp',
                             'passes_token': '--passes ', 'irefresh_type_token': '--irefresh-type ',
                             'enable_tpl_model_token':'--enable-tpl-la ', 'stats_token': '--stats'})
elif ENCODER =="x264" or ENCODER == "x265":
    encoder_cfg_list.append({'enc_name': ENCODER, 'enc_mode_token': ' --preset ', 'input_res_token': ' --input-res ',
                            'threads_token': ' --threads ', 'tune_token': ' --tune ', 'stats_token': ' --stats ',
                            'qp_value_token': ' --crf ', 'fps_token': ' --fps ', 'bitdepth_token': ' --input-depth ',
                            'bitrate_token': '--bitrate', 'maxrate_token': '--vbv-maxrate', 'bufsize_token': '--vbv-bufsize',
                            'intra_period_token': ' --keyint ', 'intra_period_token_min': ' --min-keyint ', 'no_scenecut_token': ' --no-scenecut ',
                            'bitstream_token' : ' -o ', 'frame_threads_token': ' --frame-threads', 'no_wpp_token': ' --no-wpp ', 'pools_token': '--pools'})

elif ENCODER =="vvenc":
    encoder_cfg_list.append({'enc_name': 'vvenc', 'enc_mode_token': ' --preset ', 'input_res_token': ' --size ',
                            'threads_token': ' --threads ', 'qp_value_token': ' --qp ', 'fps_token': ' --framerate ', 'bitdepth_token': ' --format ', 'bitstream_token' : ' --output ', 'input_token': ' --input ', 'intra_period_token': ' --intraperiod '})
elif ENCODER =="ffmpeg-svt":
    encoder_cfg_list.append({'enc_name': 'ffmpeg', 'enc_mode_token': ' -preset ', 'input_res_token': ' -s:v ',
                            'threads_token': ' -threads ', 'qp_value_token': ' -crf ', 'fps_token': ' -r ', 'bitdepth_token': ' -pix_fmt ', 'input_token': ' -i ', 'intra_period_token': " -g ", 'lad_token': '-la_depth ','tbr_token': ' -b:v '})
elif ENCODER =="ffmpeg-x264" or ENCODER== "ffmpeg-x265":
    encoder_cfg_list.append({'enc_name': 'ffmpeg', 'enc_mode_token': ' -preset ', 'input_res_token': ' -s:v ', 'ffmpeg_threads_token': ' -threads',
                            'threads_token': ' -threads ', 'qp_value_token': ' -qp ', 'fps_token': ' -r ', 'bitdepth_token': ' -pix_fmt ', 'input_token': ' -i ', 'intra_period_token': " -g ",
                            'x264-params_token': ' -x264-params', 'intra_period_min_token':  '-keyint_min', 'no_scenecut_token': 'scenecut=0', 'maxrate_token': '-maxrate',
                            'bufsize_token': '-bufsize', 'pools_token' : 'pools=', 'no_wpp_token': 'wpp=0','stats_token': ' -stats ', 'tune_token': ' -tune ' ,'x265-params_token': '-x265-params'})
elif ENCODER =="ffmpeg-libaom":
    encoder_cfg_list.append({'enc_name': 'ffmpeg', 'enc_mode_token': ' -preset ', 'input_res_token': ' -s:v ',
                            'qp_value_token': 'cq-level=', 'fps_token': ' -r ', 'bitdepth_token': ' -pix_fmt ', 'input_token': ' -i ', 'intra_period_token': " -g ",
                            'qp_mode_token': 'end-usage=q','enc_mode_token': '-cpu-used',
                            'auto_alt_ref_token': '-auto-alt-ref', 'lag_in_frames_token': '-lag-in-frames',
                            'use_fixed_qp_offsets_token': 'use-fixed-qp-offsets=', 'enable_tpl_model_token': 'enable-tpl-model=',
                            'min_gf_interval_token': 'min-gf-interval=', 'max_gf_interval_token': 'max-gf-interval=',
                            'gf_min_pyr_height_token': 'gf-min-pyr-height=', 'gf_max_pyr_height_token': 'gf-max-pyr-height=',
                            'aq_mode_token': 'aq-mode=', 'deltaq_token': 'deltaq-mode=', 'threads_token': ' -threads ', 'aom_params_token': "-aom-params ",
                             'intra_period_token_max': 'kf-max-dist=','passes_token': 'passes=','crf_token':' -crf '})
elif ENCODER =="ffmpeg-libvpx":
    encoder_cfg_list.append({'enc_name': 'ffmpeg', 'enc_mode_token': ' -preset ', 'input_res_token': ' -s:v ',
                            'threads_token': ' -threads ', 'qp_value_token': ' -qp ', 'fps_token': ' -r ', 'bitdepth_token': ' -pix_fmt ', 'input_token': ' -i ', 'intra_period_token': " -g ",
                            'auto_alt_ref_token': '-auto-alt-ref', 'lag_in_frames_token': '-lag-in-frames', 'qp_value_token': '-crf','enc_mode_token': '-cpu-used', 'bitrate_token': '-b:v',
                            'intra_period_min_token': " -keyint_min"})

else :
    encoder_cfg_list.append({'enc_name': 'svt-av1','config_token': ' -c ','enc_mode_token': '-enc-mode','input_token': ' -i ',
                             'width_token': '-wdt','height_token': '-hgt','qp_value_token': '-q','fps':'-fr','fps_num': '-fps-num',
                             'fps_denom': '-fps-denom','intra_period_token': ' -ip ','bitdepth_token': '-bit-depth',
                             'bitstream_token': ' -b ','num_frames_token': ' -f ','rc_token': ' -rc ','tbr_token': ' -tbr ',
                             'mbr_token': ' -mbr ', 'lad_token' : '-lad', 'lp_token': '-lp',
                             'bitdepth_token': '--bit-depth=','inbitdepth_token': '--input-bit-depth='})

####################################### =====Configuration End ===== #######################################
if metrics_computation in [0,2]:
    psnr_ssim_generation = 0
    vmaf_generation   = 0
if not downscale_commands and rescale_bdrate:
    print('ERROR: rescale_bdrate=1 not compatible with downscale=0')
    sys.exit()
if live_encoding and ENCODER not in ['svt','x264','x265','ffmpeg-svt','ffmpeg-x264','ffmpeg-x265']:
    print('CRITICAL: ENCODER %s NOT SUPPORTED BY LIVE FEATURE' % ENCODER)
    sys.exit()
if live_encoding and ENCODER in ['svt','libaom','vp9','vvenc','ffmpeg-svt','ffmpeg-libaom','ffmpeg-libvpx'] and rate_control == 2:
    print('CRITICAL: ENCODER %s DOES NOT SUPPORT CBR MODE' % ENCODER)
    sys.exit()
if not live_encoding and rate_control != 0:
    print('WARNING: YOU MAY BE USING A RISKY RATE CONTROL FOR VOD')
if rate_control!=0 and ENCODER in ['libaom']:
    print('CRITICAL: RATE CONTROL FEATURE DOES NOT SUPPORT %s' % ENCODER)
    sys.exit()
if passes != 1 and passes != 2:
    print("WARNING: "+"passes is %s. passes should be 1 or 2" % passes)
if psnr_ssim_generation != 0 and psnr_ssim_generation != 1:
    print("WARNING: "+"psnr_ssim_generation is %s. psnr_ssim_generation should be 0 or 1." % psnr_ssim_generation)
    sys.exit()
if scene_cut_clip not in [0,1]:
    print("WARNING: "+"scene_cut_clip is %s. scene_cut_clip should be 0 or 1." % scene_cut_clip)
    sys.exit()
if rescale_bdrate not in [0,1]:
    print("WARNING: "+"rescale_bdrate is %s. rescale_bdrate should be 0 or 1." % rescale_bdrate)
    sys.exit()
if downscale_commands not in [0,1]:
    print("WARNING: "+"downscale_commands is %s. downscale_commands should be 0 or 1." % downscale_commands)
    sys.exit()
if passes == 2 and (ENCODER == "x264" or ENCODER == "x265"):
    print("WARNING: 2 pass mode with x264 and x265 currently not supproted")
    sys.exit()

clip_location = CLIP_DIRS[0] + "/"

yuv_library_found = 0
try:
    from yuv_library import getyuvlist
    seq_list = getyuvlist()
    yuv_library_found = 1
except ImportError:
    print("WARNING yuv_library not found, only generating commands for y4m files.")
    seq_list = []
commands = []
if "collect_raw_bdr_speed_data" not in os.listdir("."):
    print("WARNING collect_raw_bdr_speed_data script missing")

def generate_parameters_file():
    #TODO: ignore or interpret parameters controled by if statements


    ignoreList = ["scene_changes_suffix",
                  "bitstream_folders",
                  "commands",
                  "seq_list",
                  "yuv_library_found",
                  "clip_location",
                  "encoder_cfg_list",
                  "run_all_paral_file_name",
                  "run_paral_cpu_file_name",
                  "TBR_VALUES",
                  "ffmpeg_file",
                  "vmaf_file",
                  "dec_file",
                  "fo",
                  "rescale_metrics_file",
                  "downscaled_clip_list",
                  "downscaled_clip_map",
                  "final_ffmpeg_list",
                  "COMMAND_PARAMS",
                  "cwd",
                  "test_date_time",
                  "test_date",
                  "test_distinction",
                  "debug_generate_commands",
                  "debug_run_all_paral",
                  "new_dir_path",
                  "encoder_executables",
                  "common_params",
                  "cmd",
                  "counter",
                  "curr_file_count",
                  "new_file_count"]

    cwd = os.getcwd()
    parametersParse = []

    with open(os.path.join(cwd, os.path.basename(__file__)), 'r') as file:
        for line in file.readlines():
            if line[0] == "#" or line == "\n":
                continue

            #--- skip functions ---#
            # if function_found:
            if line[0] == ' ':
                continue

            lineFound = re.findall("\w+\s*=[^=]", line.strip())

            if lineFound == []:
                continue

            # print("[DEBUG] line: %s" % line[:-1])
            # print("[DEBUG] line: %s\n" % lineFound)

            split_line = line.split('=')

            if len(split_line) < 2:
                continue

            variable = split_line[0].strip()
            value = split_line[1].strip()

            if variable in ignoreList or variable in parametersParse:
                continue

            try:
                int(value)
                parametersParse.append(variable)
            except:
                if re.findall("[\"']\w+[\"']", value) != []:  #TODO: use re to find string "..."
                    parametersParse.append(variable)
                elif re.findall("\[.*\]", value) != []:
                    parametersParse.append(variable)
                elif re.findall("\{.*\}", value) != []:
                    parametersParse.append(variable)
                elif re.findall("\w+", value) != []:
                    parametersParse.append(variable)

    #get parameters value
    param_var = []
    param_val = []
    for var in parametersParse:
        try:
            param_val.append(globals()[var])
            param_var.append(var)
        except:
            pass
    #TODO write to parameters.txt file
    log_file_name = str(os.path.basename(__file__))[:-3] + "_parameters.txt"  #FIXME assumes script is python file or file extension is 2 characters long
    parameter_log_path = os.path.join(os.getcwd(), log_file_name)

    with open(parameter_log_path, 'w+') as file:
        for key, value in zip(param_var, param_val):
            file.write("%s: %s\n" % (key, value))
        file.close()

def generate_bash_driver_file(num_pool, ffmpeg_job_count, encoder, run_all_paral_file_name, run_paral_cpu_file_name, enc_modes,cqp,scene_cut_clip, bitstream_folders):

    if cqp == 1:
        encoder_test_identifier = encoder +"-" +str(passes)+"p-cqp"
    else:
        encoder_test_identifier = encoder +"-" +str(passes)+"p"

    encoder_exec = ""
    if encoder == "svt":
        encoder_exec = "SvtAv1EncApp"
    elif encoder == "libaom":
        encoder_exec = "aomenc"
    elif encoder == "x264":
        encoder_exec = "x264"
    elif encoder == "x265":
        encoder_exec = "x265"
    elif encoder == "vp9":
        encoder_exec = "vpxenc"
    elif encoder == "ffmpeg":
        encoder_exec = "ffmpeg"
    elif encoder == "vvenc":
        encoder_exec = "vvencapp"
    elif encoder in ['ffmpeg-svt','ffmpeg-libvpx','ffmpeg-x265','ffmpeg-x264','ffmpeg-libaom']:
        encoder_exec = "ffmpeg"
    else:
        print("Warning encoder executable not specified in bash script")

    with open(run_paral_cpu_file_name, 'w') as file:
        file.write("parallel -j {} < run-{}-m$1.txt".format(num_pool, encoder_test_identifier))

    run_all_paral_script = []
    run_all_paral_script.append("#!/bin/bash")
    run_all_paral_script.append("encoder_type=\"{}\"".format(encoder))
    run_all_paral_script.append("enc_mode_array=({})".format(' '.join(map(str,enc_modes))))
    run_all_paral_script.append("debug_date=" + r"`date " + "\"+%Y-%m-%d_%H%M%S\"`")
    run_all_paral_script.append("debug_filename=" + "\"debug_output_automation_${debug_date}.txt\"")
    for folder in bitstream_folders:
        run_all_paral_script.append("mkdir {}".format(folder))
    run_all_paral_script.append("chmod +rx tools")
    run_all_paral_script.append("chmod +rx tools/*")
    run_all_paral_script.append("chmod +rx *.sh")
    run_all_paral_script.append("chmod +x {}".format(encoder_exec))
    if encoder == 'vvenc':# and metrics_computation == 2:
        run_all_paral_script.append("chmod +x vvdecapp")

    if metrics_computation == 2:
        run_all_paral_script.append('mkdir /dev/shm/bitstreams')
    if scene_cut_clip == 1 or downscale_commands == 1:
        run_all_paral_script.append("mkdir resized_clips")
    if encoder == 'ffmpeg-libaom' and passes==2:
        run_all_paral_script.append("mkdir 2pass_logs")

    if scene_cut_clip ==1:
        run_all_paral_script.append("(/usr/bin/time --verbose parallel -j {} < run_scene_cut.txt) &> time_scene_cut.log &".format(ffmpeg_job_count))
        run_all_paral_script.append("wait && echo \'Scene Cutting Finished\'")
    if downscale_commands == 1:
        run_all_paral_script.append("parallel -j {} < run_copy_reference.txt".format(ffmpeg_job_count))
        run_all_paral_script.append("(/usr/bin/time --verbose parallel -j 20 < run_downscale.txt) &> time_downscale.log &")
        run_all_paral_script.append("wait && echo \'Downscaling Finished\'")
    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")
    run_all_paral_script.append("\tif [ \"$encoder_type\" == \"libaom\" ] || [ \"$encoder_type\" == \"vp9\" ]; then")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) > time_enc_$i.log 2>&1 &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\telse")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) &> time_enc_$i.log &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\tfi\n")
    run_all_paral_script.append("\twait $b && echo \'Encoding Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    run_all_paral_script.append("done")
    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")
    if psnr_ssim_generation ==1 :
        run_all_paral_script.append("\t(parallel -j {} < run-ffmpeg-{}-m$i.txt) &> time_ffmpeg_$i.log &".format(ffmpeg_job_count, encoder_test_identifier))
        run_all_paral_script.append("\twait $b && echo \'PSNR/SSIM Generation for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    if vmaf_generation ==1 :
        run_all_paral_script.append("\t(parallel -j {} < run-vmaf-{}-m$i.txt) &> time_vmaf_$i.log &".format(ffmpeg_job_count, encoder_test_identifier))
        run_all_paral_script.append("\twait $b && echo \'VMAF Generation for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    if rescale_bdrate == 1 and metrics_computation not in [0,2]:
        run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < run-rescale-metrics-{}-m$i.txt) &> time_rescale_metrics_$i.log &".format(ffmpeg_job_count, encoder_test_identifier))
        run_all_paral_script.append("\twait $b && echo \'Rescaling VMAF/PSNR/SSIM Generation for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    if metrics_computation == 2:
        run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < run-ffmpeg-vmaf-{}-m$i.txt) &> time_vmaf_extraction_$i.log &".format(vmaf_job_count, encoder_test_identifier))
        run_all_paral_script.append("\twait $b && echo \'Running Vmaf Extraction for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    if metrics_computation == 0:
        run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < run-ffmpeg-vmaf-{}-m$i.txt) &> time_vmaf_extraction_$i.log &".format(vmaf_job_count, encoder_test_identifier))
        run_all_paral_script.append("\twait $b && echo \'Running Vmaf Extraction for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    run_all_paral_script.append("done")
    run_all_paral_script.append("python collect_raw_bdr_speed_data.py")
    # if delete_intermediary == 1:
        # run_all_paral_script.append("rm resized_clips/*.yuv")
        # run_all_paral_script.append("rm resized_clips/*.y4m")
        # run_all_paral_script.append("rm bitstreams/*.yuv")
        # run_all_paral_script.append("rm bitstreams/*.y4m")

    with open(run_all_paral_file_name, 'w') as file:
        for line in run_all_paral_script:
            file.write(line + '\n')

    ## chmodding bash files
    # NOTE: os.chmod is finicky -> may not work
    file_stat = os.stat(run_all_paral_file_name)
    os.chmod(run_all_paral_file_name, file_stat.st_mode | stat.S_IEXEC)

    file_stat = os.stat(run_paral_cpu_file_name)
    os.chmod(run_paral_cpu_file_name, file_stat.st_mode | stat.S_IEXEC)

def read_y4m_header(clip_path):

    if sys.version_info[0] ==3:
        header_delimiters = [b"W",b"H",b"F",b"I",b"A",b"C"]
    else:
        header_delimiters = ["W","H","F","I","A","C"]
    width = 0
    height = 0
    frame_ratio = ""
    framerate = 0
    number_of_frames=1
    import binascii
    #print(clip_path)
    with open(clip_path, "rb") as f:
        f.seek(10)
        if sys.version_info[0] ==3: buffer = b""
        else:   buffer=""
        while True:
            readByte = f.read(1)

            if (readByte in header_delimiters):
                if readByte == header_delimiters[0]:
                    while 1:
                        readByte = f.read(1)
                        if sys.version_info[0] ==3:
                            if (readByte == b'\n' or readByte==b' '):
                                width = int(buffer)
                                buffer = b""
                                break
                            else:
                                buffer+=readByte
                        else:
                            if (readByte == '\n' or readByte==' '):
                                width = int(buffer)
                                buffer = ""
                                break
                            else:
                                buffer+=readByte
                elif readByte == header_delimiters[1]:
                    while 1:
                        readByte = f.read(1)
                        if sys.version_info[0] ==3:
                            if (readByte == b'\n' or readByte==b' '):
                                height = int(buffer)
                                buffer = b""
                                break
                            else:
                                buffer+=readByte
                        else:
                            if (readByte == '\n' or readByte==' '):
                                height = int(buffer)
                                buffer = ""
                                break
                            else:
                                buffer+=readByte
                elif readByte == header_delimiters[2]:
                    while 1:
                        readByte = f.read(1)
                        if sys.version_info[0] ==3:
                            if (readByte == b'\n' or readByte==b' '):
                                frame_ratio = buffer
                                buffer = b""
                                break
                            else:
                                buffer+=readByte
                        else:
                            if (readByte == '\n' or readByte==' '):
                                frame_ratio = buffer
                                buffer = ""
                                break
                            else:
                                buffer+=readByte
                elif readByte == header_delimiters[5]:
                    while 1:
                        readByte = f.read(1)

                        if sys.version_info[0] ==3:
                            if (readByte == b'\n' or readByte==b' '):
                                if b'=' in buffer:
                                    break
                                if b'10' in buffer:
                                    bit_depth='10bit'
                                else:
                                    bit_depth='8bit'
                                #bit_depth = buffer
                                buffer = b""
                                break
                            else:
                                buffer+=readByte
                        else:

                            if (readByte == '\n' or readByte==' '):
                                if '=' in buffer:
                                    break
                                if '10' in buffer:
                                    bit_depth='10bit'
                                else:
                                    bit_depth='8bit'
                                #bit_depth = buffer
                                buffer = ""
                                break
                            else:
                                buffer+=readByte
            if sys.version_info[0] ==3:
                if binascii.hexlify(readByte) == b'0a':
                    break
            else:
                if binascii.hexlify(readByte) == '0a':
                    break
        # other_counter=0
        # print('width',width)
        # print('height',height)
        # print('bit_depth',bit_depth)
        # print('os.path.getsize(clip_path)/60',os.path.getsize(clip_path)/60)
        # print('os.path.getsize(clip_path))',os.path.getsize(clip_path))
        if bit_depth=='10bit':
            frame_length = int(float(2)*float(width * height * float(3) / 2))
            y4m_bitdepth = 10
        else:
            frame_length = int(width * height * float(3) / 2)
            y4m_bitdepth = 8
        # print('frame_length',frame_length)
        #frame_length=6220807
        # print(os.path.getsize(clip_path))
        if intra_period == -1 and ENCODER != 'svt':
            while f.tell() < os.path.getsize(clip_path):
                readByte = f.read(1)
                if binascii.hexlify(readByte) == b'0a':
                    # other_counter+=1
                    f.seek(frame_length,1)
                    buff=binascii.hexlify(f.read(5))
                    #print('buff',buff)
                    if buff == b'4652414d45':
                        number_of_frames+=1
        # print('other_counter',other_counter)
        #print('number_of_frames',number_of_frames)
##        frame_ratio_pieces = frame_ratio.split(":")
##        framerate = float(frame_ratio_pieces[0])/float(frame_ratio_pieces[1])
##        print('width',width)
##        print('height',height)
##        print('framerate',framerate)

    return width, height, framerate, number_of_frames, y4m_bitdepth


def get_fps(clip_dir, clip):
    if(".yuv" in clip and yuv_library_found):
        seq_table_index = get_seq_table_loc(seq_list, clip)
        if seq_table_index < 0:
            return 0
        fps = float(seq_list[seq_table_index]["fps_num"]) / seq_list[seq_table_index]["fps_denom"]
        return fps
    elif(".y4m" in clip):
        _, _, framerate,number_of_frames,y4m_bitdepth = read_y4m_header(os.path.join(clip_dir, clip))
        return framerate
    else:
        return 0

def get_clip_list(clip_dir):
    files = os.listdir(clip_dir)
    files.sort(key=lambda f: get_fps(clip_dir, f), reverse=True)
    files.sort(key=lambda f: f.lower())
    files.sort(key=lambda f: (os.stat(os.path.join(clip_dir, f)).st_size), reverse=True)
    Y4M_HEADER_SIZE = 80

    ## sort all files in lists by size
    clip_lists = [[]]
    clip_list = []
    size_0 = os.stat(os.path.join(clip_dir, files[0])).st_size
    for file_ in files:
        if (".yuv" in file_ or ".y4m" in file_):
            if ".yuv" in file_ and os.stat(os.path.join(clip_dir, file_)).st_size == size_0:
                clip_list.append(file_)
            elif ".y4m" in file_ and size_0 - Y4M_HEADER_SIZE<= os.stat(os.path.join(clip_dir, file_)).st_size<= size_0+Y4M_HEADER_SIZE:
                clip_list.append(file_)
            else:
                clip_lists.append(clip_list)
                clip_list = []
                size_0 = os.stat(os.path.join(clip_dir, file_)).st_size
                clip_list.append(file_)

    clip_lists.append(clip_list)
    return clip_lists

def get_seq_table_loc(seq_table, clipname):

    for i in range(len(seq_table)):
        if seq_table[i]["name"] == clipname[:-4]:
            return i


    return -1


def generate_commands(CLIP_DIRS, clip_location, ENC_MODES, passes, cqp, psnr_ssim_generation, vmaf_generation, disable_enhancement_vmaf,
                      scene_cut_clip, scene_changes_suffix, downscale_commands, rescale_bdrate, downscale_target_resolutions,
                      bitstreams, ENCODER, seq_list):

    ffmpeg_file = []
    ffmpeg_vmaf_file = []
    vmaf_file = []
    fo = []
    rescale_metrics_file = []

    scene_cut_clips = [[]]
    scene_cut_map = {}

    if scene_cut_clip:
        with open("run_scene_cut.txt", "w") as g:
            for clip_directory in CLIP_DIRS:
                clip_lists = get_clip_list(clip_directory)
                for i in range(len(clip_lists)):
                    for clip_ in clip_lists[i]:
                        seq_table_index = get_seq_table_loc(seq_list, clip_)
                        if seq_table_index < 0 and ".yuv" in clip_:
                            continue
                        elif ".y4m" not in clip_ and ".yuv" not in clip_:
                            continue

                        if ".yuv" in clip_:
                            seq_list_entry = seq_list[seq_table_index]
                            width = seq_list_entry["width"]
                            height = seq_list_entry["height"]
                            bit_depth = seq_list_entry["bitdepth"]
                            ten_bit_format = seq_list_entry["unpacked"]

                            if bit_depth == 10:
                                pix_fmt = "yuv420p10le"
                                if ten_bit_format == 2:
                                    total_number_of_frames = (int)(((float)(os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/1.25)
                                else:
                                    total_number_of_frames = (int)(((os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/2)
                            elif bit_depth == 8:
                                pix_fmt = "yuv420p"
                                total_number_of_frames = (int)(os.path.getsize(os.path.join(clip_directory,clip_))/(width*height+(width*height/2)))



                        try:
                            with open(clip_directory+"/"+clip_[:-4]+scene_changes_suffix, "r") as f:
                                scene_cuts = f.readlines()
                                for i in range(len(scene_cuts)):
                                    try:
                                        frame_start = int(scene_cuts[i])
                                        if ".yuv" in clip_ and frame_start > total_number_of_frames:
                                            print("ERROR: cutting scene at frame {}, beyond length of the clip {}".format(frame_start, total_number_of_frames))
                                            quit()
                                    except:
                                        print("{} could not be converted into an integer. Exiting".format(scene_cuts[i]))
                                        quit()
                                    if i < len(scene_cuts)-1:
                                        try:
                                            frame_end = int(scene_cuts[i+1])-1
                                        except:
                                            print("{} could not be converted into an integer. Exiting".format(scene_cuts[i+1]))
                                            quit()
                                    else:
                                        frame_end = total_number_of_frames

                                    new_clip_name = clip_[:-4] + "_scene_{}".format(i) + clip_[-4:]

                                    target_path = "resized_clips/" + new_clip_name

                                    ffmpeg_string = "tools/ffmpeg"

                                    if ".yuv" in clip_:

                                        new_entry = deepcopy(seq_list_entry)
                                        new_entry["name"] = new_clip_name[:-4]
                                        new_entry["number_of_frames"] = frame_end - frame_start + 1

                                        seq_list.append(new_entry)

                                        ffmpeg_params = " -y -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 -i {} -vf select=\'between(n\\,{}\\,{})\' -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 {}".format(width, height,\
                                                        pix_fmt, os.path.join(clip_location, clip_), int(frame_start)-1, int(frame_end)-1, width, height, pix_fmt, target_path)

                                        if i == len(scene_cuts)-1:
                                            ffmpeg_params = " -y -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 -i {} -vf select=\'gte(n\\,{})\' -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 {}".format(width, height, pix_fmt, os.path.join(clip_location, clip_), int(frame_start)-1, width, height, pix_fmt, target_path)

                                    else:
                                        width, height, _,number_of_frames,y4m_bitdepth = read_y4m_header(os.path.join(clip_directory,clip_))
                                        ffmpeg_params = " -y -i {} -vf select=\'between(n\\,{}\\,{})\' {}".format(os.path.join(clip_location, clip_), int(frame_start)-1, int(frame_end)-1, target_path)
                                        if i == len(scene_cuts)-1:
                                            ffmpeg_params = " -y -i {} -vf select=\'gte(n\\,{})\' {}".format(os.path.join(clip_location, clip_), int(frame_start)-1, target_path)

                                    scene_cut_map[new_clip_name] = {"width": width, "height": height}
                                    scene_cut_clips[0].append(new_clip_name)
                                    cutting_command = ffmpeg_string+ ffmpeg_params
                                    g.write(cutting_command+"\n")
                        except:
                            print("Could not find associated scenes file for the clip {}. Exiting".format(clip_))
                            quit()

    downscaled_clip_list = []
    downscaled_clip_map = {}
    frame_tracker = {}
    if downscale_commands:
        work_env = os.getcwd()

        for target_res in downscale_target_resolutions:
            downscaled_clip_map["{}x{}".format(target_res[0], target_res[1])] = []
        with open("run_downscale.txt", "w") as f, open("run_copy_reference.txt", "w") as g:

            if ENCODER == "svt-av1": # Implemented for abbreviation purposes
                encode_file_name = "svt"
            else:
                encode_file_name = str(ENCODER)

            if scene_cut_clip == 1:
                resize_dir = "resized_clips/"
                CLIP_DIRS = [resize_dir]
                clip_location = resize_dir

            for clip_directory in CLIP_DIRS:
                if scene_cut_clip:
                    clip_lists = scene_cut_clips
                else:
                    clip_lists = get_clip_list(clip_directory)

                for i in range(len(clip_lists)):
                    for clip_ in clip_lists[i]:
                        seq_table_index = get_seq_table_loc(seq_list, clip_)
                        if seq_table_index < 0 and ".yuv" in clip_:
                            continue
                        elif ".y4m" not in clip_ and ".yuv" not in clip_:
                            continue

                        if "yuv" in clip_:
                            width = seq_list[seq_table_index]['width']
                            height = seq_list[seq_table_index]['height']
                            ten_bit_format = seq_list[seq_table_index]['unpacked']
                            bit_depth = seq_list[seq_table_index]['bitdepth']


                            if bit_depth == 10:
                                pix_fmt = "yuv420p10le"
                                if scene_cut_clip == 1:
                                    number_of_frames = seq_list[seq_table_index]['number_of_frames']
                                else:
                                    if ten_bit_format == 2:
                                        number_of_frames = (int)(((float)(os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/1.25)
                                    else:
                                        number_of_frames = (int)(((os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/2)

                            elif bit_depth == 8:
                                pix_fmt = "yuv420p"
                                if scene_cut_clip == 1:
                                    number_of_frames = seq_list[seq_table_index]['number_of_frames']
                                else:
                                    number_of_frames = (int)(os.path.getsize(os.path.join(clip_directory,clip_))/(width*height+(width*height/2)))

                            seq_list_entry = seq_list[seq_table_index]

                        if scene_cut_clip == 1:
                            width, height = scene_cut_map[clip_]["width"],scene_cut_map[clip_]["height"]
                        elif "y4m" in clip_:
                            width, height, _,number_of_frames,y4m_bitdepth = read_y4m_header(os.path.join(clip_directory,clip_))
                            frame_tracker[clip_]=number_of_frames

                        match = re.search(r"_\d+x\d+_?",clip_)
                        if match is None:
                            match = re.search(r"_\d+p.*?_",clip_)
                            if match is None:
                                match_start = len(clip_)-4
                                match_end = len(clip_)-4
                            else:
                                match_start = match.start()
                                match_end = match.end()
                        else:
                            match_start = match.start()
                            match_end = match.end()
                        
                        res_frag = "_{}x{}".format(width,height)
                        if match_start != match_end and clip_[match_start:match_end][-1] == "_":
                            res_frag = res_frag + "_"
                        ref_clip = clip_[:match_start] + res_frag + clip_[match_end:]
                        if "{}x{}".format(width,height) not in downscaled_clip_map:
                            downscaled_clip_map["{}x{}".format(width, height)] = []

                        downscaled_clip_map["{}x{}".format(width, height)].append(ref_clip)

                        if not scene_cut_clip:
                            cp_command = "cp {} {}".format(os.path.join(clip_location, clip_), "resized_clips/"+ ref_clip +"\n")
                            g.write(cp_command)

                        if "yuv" in clip_:
                            seq_list[seq_table_index]["name"] = ref_clip[:-4]
                            seq_list[seq_table_index]["number_of_frames"] = number_of_frames

                        for target_res in downscale_target_resolutions:
                            target_width = target_res[0]
                            target_height = target_res[1]
                            if target_width >= width and target_height >= height:
                                continue
                            new_clip_name = clip_[:match_start] + "_{}x{}to{}x{}_{}_".format(width, height, target_width, target_height,downscale_algo[:4]) + clip_[match_end:]
                            downscaled_clip_map["{}x{}".format(target_width, target_height)].append(new_clip_name)
                            #print('new_clip_name',new_clip_name)
                            #print('clip_',clip_)
                            #print('ref_clip',ref_clip)
                            frame_tracker[new_clip_name]=number_of_frames
                            frame_tracker[clip_]=number_of_frames
                            frame_tracker[ref_clip]=number_of_frames

                            target_path = "resized_clips/" + new_clip_name

                            if downscale_converter == 1:
                                ffmpeg_string = "tools/ffmpeg"

                                if ".yuv" in clip_:
                                    seq_list_entry = seq_list[seq_table_index]
                                    new_entry = deepcopy(seq_list_entry)
                                    new_entry["name"] = new_clip_name[:-4]
                                    new_entry["width"] = target_width
                                    new_entry["height"] = target_height
                                    new_entry["number_of_frames"] = number_of_frames

                                    seq_list.append(new_entry)

                                    ffmpeg_params = " -y -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 -i {} -sws_flags {}+accurate_rnd+full_chroma_int -sws_dither none -param0 5 -strict -1 -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 {}".format(width, height, pix_fmt, os.path.join(clip_location, clip_), downscale_algo, target_width, target_height, pix_fmt, target_path)
                                else:
                                    ffmpeg_params = " -y -i {} -sws_flags {}+accurate_rnd+full_chroma_int -sws_dither none -param0 5 -strict -1 -s:v {}x{} {}".format(os.path.join(clip_location, clip_), downscale_algo, target_width, target_height, target_path)

                                downscale_command = ffmpeg_string+ ffmpeg_params
                                f.write(downscale_command+"\n")
                            else:
                                hdr_string = "tools/HDRConvert"

                                if ".yuv" in clip_:
                                    seq_list_entry = seq_list[seq_table_index]
                                    new_entry = deepcopy(seq_list_entry)
                                    new_entry["name"] = new_clip_name[:-4]
                                    new_entry["width"] = target_width
                                    new_entry["height"] = target_height
                                    new_entry["number_of_frames"] = number_of_frames

                                    seq_list.append(new_entry)
                                    hdr_params = ''' -p SourceFile="%s" -p OutputFile="%s" -p SourceWidth=%s -p SourceHeight=%s -p OutputWidth=%s -p OutputHeight=%s -p SourceBitDepthCmp0=%s -p SourceBitDepthCmp1=%s -p SourceBitDepthCmp2=%s -p OutputBitDepthCmp0=%s -p OutputBitDepthCmp1=%s -p OutputBitDepthCmp2=%s -p SourceRate=%s -p NumberOfFrames=%s -p OutputRate=%s -p SourceChromaFormat=1 -p SourceFourCCCode=0 -p SourceColorSpace=0 -p SourceColorPrimaries=1 -p OutputChromaFormat=1 -p OutputColorSpace=0 -p OutputColorPrimaries=1 -p SilentMode=1 -p ScaleOnly=1''' % (os.path.join(clip_location, clip_), target_path, width, height, target_width, target_height, bit_depth, bit_depth, bit_depth, bit_depth, bit_depth, bit_depth, clip_fps, number_of_frames, clip_fps)
                                else:
                                    hdr_params = ''' -p SourceFile="%s" -p OutputFile="%s" -p OutputWidth=%s -p OutputHeight=%s -p NumberOfFrames=1000 -p SilentMode=1 -p ScaleOnly=1''' % (os.path.join(clip_location, clip_), target_path, target_width, target_height)

                                downscale_command = hdr_string+ hdr_params.replace('\n','')
                                f.write(downscale_command+"\n")

        key_list = downscaled_clip_map.keys()
        #key_list.sort(key=lambda k: int(k.split("x")[0]) * int(k.split("x")[1]), reverse=True)
        key_list=sorted(key_list,key=lambda k: int(k.split("x")[0]) * int(k.split("x")[1]), reverse=True)
        downscaled_clip_list.append(downscaled_clip_map[key_list[0]])
        for target_res in downscale_target_resolutions:
            downscaled_clip_list.append(downscaled_clip_map["{}x{}".format(target_res[0], target_res[1])])

    else:

        for clip_directory in CLIP_DIRS:
            if scene_cut_clip:
                clip_lists = scene_cut_clips
            else:
                clip_lists = get_clip_list(clip_directory)

            for i in range(len(clip_lists)):
                for clip_ in clip_lists[i]:
                    seq_table_index = get_seq_table_loc(seq_list, clip_)
                    if seq_table_index < 0 and ".yuv" in clip_:
                        continue
                    elif ".y4m" not in clip_ and ".yuv" not in clip_:
                        continue

                    if "y4m" in clip_:
                        width, height, _,number_of_frames,y4m_bitdepth = read_y4m_header(os.path.join(clip_directory,clip_))
                        frame_tracker[clip_]=number_of_frames


    for enc_mode in ENC_MODES:
        # create all the txt files
        if cqp == 1:
            COMMANDS_FILE_NAME = "run-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
        else:
            COMMANDS_FILE_NAME = "run-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"

        fo.append(open(COMMANDS_FILE_NAME, "w"))
        if psnr_ssim_generation == 1 and rescale_bdrate != 1:
                if cqp == 1:
                    FFMPEG_FILE_NAME = "run-ffmpeg-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
                else:
                    FFMPEG_FILE_NAME = "run-ffmpeg-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"
                ffmpeg_file.append(open(FFMPEG_FILE_NAME, "w"))
        if metrics_computation in [0,2]:
                if cqp == 1:
                    FFMPEG_VMAF_FILE_NAME = "run-ffmpeg-vmaf-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
                else:
                    FFMPEG_VMAF_FILE_NAME = "run-ffmpeg-vmaf-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"
                ffmpeg_vmaf_file.append(open(FFMPEG_VMAF_FILE_NAME, "w"))
        if vmaf_generation == 1:
            if cqp == 1:
                VMAF_FILE_NAME = "run-vmaf-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
            else:
                VMAF_FILE_NAME = "run-vmaf-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"
            vmaf_file.append(open(VMAF_FILE_NAME, "w"))

        if rescale_bdrate == 1 and metrics_computation not in [0,2]:


            if cqp == 1:
                RESCALE_METRICS = "run-rescale-metrics-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
            else:
                RESCALE_METRICS = "run-rescale-metrics-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"
            rescale_metrics_file.append(open(RESCALE_METRICS, "w"))

        for bitstream_folder in bitstream_folders:

            if downscale_commands or scene_cut_clip:
                resize_dir = "resized_clips/"
                CLIP_DIRS = [resize_dir]
                clip_location = resize_dir

            for clip_directory in CLIP_DIRS:

                if downscale_commands:
                    clip_lists = downscaled_clip_list
                elif scene_cut_clip:
                    clip_lists = scene_cut_clips
                else:
                    clip_lists = get_clip_list(clip_directory)
                #print('clip_lists',clip_lists)
                for i in range(len(clip_lists)):
                    for rc_value in RC_VALUES:
                        rc_value_back = rc_value
                        for clip_ in clip_lists[i]:
                            rc_value = rc_value_back
                            #print(bitstream_folder)
                            #print('enc_mode',enc_mode)
                            #print('CLIP_DIRS',CLIP_DIRS)
                            #print('clip_lists',clip_lists)
                            #print('i',i)
                            #print('clip_',clip_)

                            seq_table_index = get_seq_table_loc(seq_list, clip_)
                            if seq_table_index < 0 and ".yuv" in clip_:
                                print('we_skipped_here')
                                continue
                            elif ".y4m" not in clip_ and ".yuv" not in clip_ and ".mp4" not in clip_:
                                continue
                            if yuv_library_found and ".yuv" in clip_:
                                bit_depth = seq_list[seq_table_index]['bitdepth']
                                ten_bit_format = seq_list[seq_table_index]['unpacked']
                                width = seq_list[seq_table_index]['width']
                                height = seq_list[seq_table_index]['height']

                                if (bit_depth == 8):

                                    config_used ="encoder_randomaccess_main.cfg"
                                    if downscale_commands or scene_cut_clip:
                                        number_of_frames = seq_list[seq_table_index]['number_of_frames']
                                    else:
                                        number_of_frames = (int)(os.path.getsize(os.path.join(clip_directory,clip_))/(width*height+(width*height/2)))

                                elif (bit_depth == 10):

                                    config_used = "encoder_randomaccess_main_10bit.cfg"
                                    if downscale_commands or scene_cut_clip:
                                        number_of_frames = seq_list[seq_table_index]['number_of_frames']
                                    elif ten_bit_format == 2:
                                        number_of_frames = (int)(((float)(os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/1.25)
                                    else:
                                        number_of_frames = (int)(((os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/2)
                            elif ".y4m" in clip_:
                                # print(frame_tracker)
                                number_of_frames = frame_tracker[clip_]

                            output_encoder = ENCODER

                            if crf_based_target_bitrate:
                                CRF_SEARCH_STRING = os.path.join(CRF_DIR[0], output_encoder + "_M" + str(enc_mode) + "_" + clip_[:-4] +"_Q"+str(rc_value) + ".txt")
                                with open(CRF_SEARCH_STRING, "r") as f:
                                    lines = f.readlines()
                                    for raw_line in lines:
                                        line = str(raw_line).lower()
                                        if "bps" in line :
                                            bitrate_line = line
                                    bitrate_pieces = bitrate_line.split()
                                    rc_value = int(float (bitrate_pieces[4]))

                            if passes == 2:
                                output_encoder = ENCODER + "_2p"
                            if not rate_control:
                                rc_value_string = "_Q" + str(rc_value)
                            else:
                                rc_value_string = "_TBR" + str(rc_value)

                            output_string = os.path.join(bitstream_folder, output_encoder + "_M" + str(enc_mode) + "_" + clip_[:-4] + rc_value_string)

                            command = []
                            second_command = []

                            if ENCODER == "libaom":
                                command.append("(/usr/bin/time --verbose   ./aomenc")
                                if passes in [1,2]:
                                    command.append(encoder_cfg_list[0]['passes_token']+ str(passes))

                                command.append("--verbose ")
                                command.append(encoder_cfg_list[0]['lag_in_frames_token'] + "35")
                                command.append(encoder_cfg_list[0]['auto_alt_ref_token'] + "1")
                                command.append(encoder_cfg_list[0]['qp_mode_token'])
                                if passes == 1 and cqp == 1:
                                    command.append(encoder_cfg_list[0]['use_fixed_qp_offsets_token'] + "1")
                                    command.append(encoder_cfg_list[0]['enable_tpl_model_token'] + "0")
                                    command.append(encoder_cfg_list[0]['min_gf_interval_token'] + "16")
                                    command.append(encoder_cfg_list[0]['max_gf_interval_token'] + "16")
                                    command.append(encoder_cfg_list[0]['gf_min_pyr_height_token'] + "4")
                                    command.append(encoder_cfg_list[0]['gf_max_pyr_height_token'] + "4")
                                    command.append(encoder_cfg_list[0]['aq_mode_token'] + "0")
                                    command.append(encoder_cfg_list[0]['deltaq_token'] + "0")
                                if ".yuv" in clip_:
                                    command.append(
                                        encoder_cfg_list[0]['bitdepth_token'] + str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['inbitdepth_token']+ str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['width_token'] + str(seq_list[seq_table_index]['width']))
                                    command.append(encoder_cfg_list[0]['height_token'] + str(seq_list[seq_table_index]['height']))
                                    command.append(encoder_cfg_list[0]['fps_token'] + str(seq_list[seq_table_index]['fps_num']) + '/' + str(seq_list[seq_table_index]['fps_denom']))
                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                    command.append(encoder_cfg_list[0]["intra_period_token_max"] + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                    command.append(encoder_cfg_list[0]["intra_period_token_max"] + str(intra_period))

                                command.append(encoder_cfg_list[0]['qp_value_token'] + str(str(rc_value)))
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + str(enc_mode))
                                command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin")
                                command.append(encoder_cfg_list[0]['input_token'] + " " + os.path.join(clip_location,clip_)+"  ) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "svt":

                                command.append("(/usr/bin/time --verbose   ./SvtAv1EncApp")
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + " " + str(enc_mode))
                                if passes == 2:
                                    command.append(encoder_cfg_list[0]["passes_token"] + "2")
                                    command.append(encoder_cfg_list[0]["irefresh_type_token"] + "2")
                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]['width_token'] + " " + str(seq_list[seq_table_index]['width']))
                                    command.append(encoder_cfg_list[0]['height_token'] + " " + str(seq_list[seq_table_index]['height']))
                                    command.append(encoder_cfg_list[0]['bitdepth_token'] + " " + str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['fps_num'] + " " + str(seq_list[seq_table_index]['fps_num']))
                                    command.append(encoder_cfg_list[0]['fps_denom'] + " " + str(seq_list[seq_table_index]['fps_denom']))
                                    if not scene_cut_clip == 1:
                                        command.append(encoder_cfg_list[0]['num_frames_token'] + " " + str(number_of_frames))

                                if live_encoding:

                                    command.append(encoder_cfg_list[0]['rc_token'] + " " + str(1))
                                    command.append(encoder_cfg_list[0]['tbr_token'] + " " + str(str(rc_value)))
                                else:
                                    if rate_control == 0:
                                        command.append(encoder_cfg_list[0]["qp_value_token"] + " " + str(rc_value))
                                    else:
                                        command.append(encoder_cfg_list[0]['rc_token'] + " " + str(1))
                                        command.append(encoder_cfg_list[0]['tbr_token'] + " " + str(str(rc_value)))

                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                command.append(encoder_cfg_list[0]['intra_period_token'] + " " + str(intra_period))

                                # if passes == 2 or cqp == 0 :
                                    # command.append(encoder_cfg_list[0]["enable_tpl_model_token"] + "1")
                                # else :
                                if cqp:
                                    #command.append(encoder_cfg_list[0]['lad_token'] + " " + "0")
                                    command.append(encoder_cfg_list[0]["enable_tpl_model_token"] + "0")

                                # command.append(" --pic-based-rate-est 1")
                                #command.append(encoder_cfg_list[0]['lad_token'] + " " + "0")

                                if lp_number != -1:
                                    command.append(encoder_cfg_list[0]['lp_token'] + " " + str(lp_number))

                                command.append(encoder_cfg_list[0]['input_token'] + " " + os.path.join(clip_location,clip_))
                                if passes == 1:
                                    command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin ) ")
                                    command.append("> " + output_string + ".txt" + " 2>&1")
                                elif passes == 2:
                                    command.append(encoder_cfg_list[0]['stats_token'] + output_string + ".stat")
                                    command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin ) ")
                                    command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "x264":
                                # output_string = ENCODER + "_M" + str(enc_mode) + "_" + clip_[:-4] + "_Q" + str(qp_value)
                                command.append("(/usr/bin/time --verbose   ./x264")

                                command.append(encoder_cfg_list[0]["enc_mode_token"] + str(enc_mode))

                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]["input_res_token"] + str(seq_list[seq_table_index]['width']) + "x" + str(seq_list[seq_table_index]['height']))
                                    command.append(encoder_cfg_list[0]["fps_token"] + str(seq_list[seq_table_index]['fps_num']) + "/" + str(seq_list[seq_table_index]['fps_denom']))
                                    command.append(encoder_cfg_list[0]["bitdepth_token"] + str(seq_list[seq_table_index]['bitdepth']))
                                if not live_encoding: command.append(encoder_cfg_list[0]["threads_token"] + "1")
                                command.append(encoder_cfg_list[0]["tune_token"] + "psnr")


                                command.append(encoder_cfg_list[0]["stats_token"]+output_string+".stat")

                                if live_encoding:
                                    if rate_control == 2:

                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["bufsize_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["maxrate_token"] + " " + str(rc_value))

                                    elif rate_control == 1:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " +str(rc_value))

                                else:
                                    if rate_control == 2:

                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["bufsize_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["maxrate_token"] + " " + str(rc_value))

                                    elif rate_control == 1:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " +str(rc_value))
                                    elif rate_control == 0:
                                        command.append(encoder_cfg_list[0]["qp_value_token"] + str(rc_value))


                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                    command.append(encoder_cfg_list[0]["intra_period_token_min"] + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                    command.append(encoder_cfg_list[0]["intra_period_token_min"] + str(intra_period))
                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                if no_scenecut != -1:
                                    command.append(encoder_cfg_list[0]['no_scenecut_token'])

                                command.append(encoder_cfg_list[0]["bitstream_token"] + output_string + ".bin")
                                command.append(" " + os.path.join(clip_location, clip_) + ") ")
                                command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "x265":
                                # output_string = ENCODER + "_M" + str(enc_mode) + "_" + clip_[:-4] + "_Q" + str(rc_value)
                                command.append("(/usr/bin/time --verbose   ./x265")

                                command.append(encoder_cfg_list[0]["enc_mode_token"] + str(enc_mode))

                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]["input_res_token"]+ str(seq_list[seq_table_index]['width']) + "x" + str(seq_list[seq_table_index]['height']))
                                    command.append(encoder_cfg_list[0]["fps_token"] + str(seq_list[seq_table_index]['fps_num']) + "/" + str(seq_list[seq_table_index]['fps_denom']))
                                    command.append(encoder_cfg_list[0]["bitdepth_token"]+ str(seq_list[seq_table_index]['bitdepth']))

                                if pools != -1:
                                    command.append(encoder_cfg_list[0]['pools_token'] + " " + str(pools))
                                command.append(encoder_cfg_list[0]["tune_token"]+ " psnr")
                                command.append(encoder_cfg_list[0]["stats_token"]+output_string+".stat")

                                if live_encoding:
                                    if rate_control == 2:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["bufsize_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["maxrate_token"] + " " + str(rc_value))
                                    elif rate_control == 1:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " +str(rc_value))
                                else:
                                    if rate_control == 2:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["bufsize_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["maxrate_token"] + " " + str(rc_value))
                                    elif rate_control == 0:
                                        command.append(encoder_cfg_list[0]["qp_value_token"] + str(rc_value))
                                    elif rate_control == 1:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " +str(rc_value))

                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                    command.append(encoder_cfg_list[0]["intra_period_token_min"] + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                    command.append(encoder_cfg_list[0]["intra_period_token_min"] + str(intra_period))

                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                if not live_encoding: command.append(encoder_cfg_list[0]["pools_token"]+ " " + "1")
                                #if not live_encoding: command.append(encoder_cfg_list[0]["frame_threads_token"]+ " " + "1")

                                if no_scenecut != -1:
                                    command.append(encoder_cfg_list[0]['no_scenecut_token'])

                                if not live_encoding: command.append(encoder_cfg_list[0]["no_wpp_token"])

                                command.append(" " + os.path.join(clip_location, clip_))
                                command.append(encoder_cfg_list[0]["bitstream_token"] + output_string + ".bin) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "vp9":
                                command.append("(/usr/bin/time --verbose   ./vpxenc")
                                '''efluente tokens'''
                                command.append(' --ivf --codec=vp9  --tile-columns=0 --arnr-maxframes=7 --arnr-strength=5 --aq-mode=0 --bias-pct=100 --minsection-pct=1 --maxsection-pct=10000 --i420 --min-q=0 --frame-parallel=0 --min-gf-interval=4 --max-gf-interval=16')
                                ''''''
                                command.append("--verbose ")
                                if passes in [1,2]:
                                    command.append(encoder_cfg_list[0]["passes_token"] + str(passes))
                                command.append(encoder_cfg_list[0]["qp_mode_token"])
                                command.append(encoder_cfg_list[0]["lag_in_frames_token"] + "25")
                                command.append(encoder_cfg_list[0]["auto_alt_ref_token"] + "6")
                                command.append(encoder_cfg_list[0]["threads_token"] + "1")
                                if ".yuv" in clip_:
                                    if int(seq_list[seq_table_index]['bitdepth']) == 8:
                                        command.append( " --profile=0 " )
                                    else :
                                        command.append( " --profile=2 " )
                                    command.append(encoder_cfg_list[0]['bitdepth_token'] + str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['inbitdepth_token']+ str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['width_token'] + str(seq_list[seq_table_index]['width']))
                                    command.append(encoder_cfg_list[0]['height_token'] + str(seq_list[seq_table_index]['height']))
                                    command.append(
                                        encoder_cfg_list[0]['fps_token'] + str(seq_list[seq_table_index]['fps_num']) + '/' + str(
                                            seq_list[seq_table_index]['fps_denom']))
                                elif clip_.endswith('y4m'):
                                    if int(y4m_bitdepth) == 8:
                                        command.append( " --profile=0 " )
                                    else :
                                        command.append( " --profile=2 " )
                                        command.append(encoder_cfg_list[0]['bitdepth_token'] + '10 ')
                                        command.append(encoder_cfg_list[0]['inbitdepth_token'] + '10 ')
                                # command.append(encoder_cfg_list[0]['intra_period_token'] + str(intra_period))
                                # command.append(encoder_cfg_list[0]['intra_period_token_max'] + str(intra_period))
                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                    command.append(encoder_cfg_list[0]["intra_period_token_max"] + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                    command.append(encoder_cfg_list[0]["intra_period_token_max"] + str(intra_period))
                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                command.append(encoder_cfg_list[0]['qp_value_token'] + str(str(rc_value)))
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + str(enc_mode))
                                command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin")
                                command.append(encoder_cfg_list[0]['input_token'] + " " + os.path.join(clip_location,clip_)+"  ) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "vvenc":
                                command.append("(/usr/bin/time --verbose ./vvencapp")
                                command.append(encoder_cfg_list[0]['input_token'] + os.path.join(clip_location, clip_))
                                command.append(encoder_cfg_list[0]['input_res_token'] + "{}x{}".format(str(seq_list[seq_table_index]['width']), str(seq_list[seq_table_index]['height'])))
                                command.append(encoder_cfg_list[0]['qp_value_token'] + str(rc_value))
                                vvenc_presets = ["slow", "medium", "fast", "faster"]
                                if enc_mode < 4:
                                    preset = vvenc_presets[enc_mode]
                                else:
                                    preset = "faster"
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + preset)
                                if int(seq_list[seq_table_index]['bitdepth']) == 10:
                                    command.append(encoder_cfg_list[0]['bitdepth_token'] + " yuv420_10")
                                    command.append(' --internal-bitdepth 10 ')
                                else:
                                    command.append(encoder_cfg_list[0]['bitdepth_token'] + " yuv420")
                                    command.append(' --internal-bitdepth 8 ')

                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                #command.append(encoder_cfg_list[0]['intra_period_token'] + str(intra_period))
                                command.append(encoder_cfg_list[0]['fps_token'] + str(float(seq_list[seq_table_index]['fps_num'])/float(seq_list[seq_table_index]['fps_denom'])))
                                command.append(encoder_cfg_list[0]['threads_token'] + "1")
                                command.append(encoder_cfg_list[0]['bitstream_token'] + output_string +".bin ) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")


                            elif ENCODER == "ffmpeg-svt":
                                if ffmpeg_rescale_abr:#######################################################################################Check if this works later############################################################################################
                                    command='''(/usr/bin/time --verbose ./ffmpeg -i %s'''% (os.path.join(clip_location,clip_))
                                    for sub_list in ffmpeg_rescale_abr_target_resolutions:
                                        width=sub_list[0].split('x')[0]
                                        height=sub_list[0].split('x')[1]
                                        command+='''-c:v libsvtav1 -preset %s -b:v %sk -vf scale=%s:%s -y %s_%sx%s.ivf '''% (enc_mode,sub_list[1],width,height,clip_.split('.')[-2].split('\\')[-1],width,height)
                                    command+='-abr_pipeline -filter_scale_threads 16 ) > time_total_abr_scale.txt 2>&1 &'
                                    command=command.split(' ')
                                command.append("(/usr/bin/time --verbose ./ffmpeg -y")
                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]['input_res_token'] + "{}x{}".format(str(seq_list[seq_table_index]['width']), str(seq_list[seq_table_index]['height'])))
                                    if int(seq_list[seq_table_index]['bitdepth']) == 10:
                                        command.append(encoder_cfg_list[0]['bitdepth_token'] + "yuv420p10le")
                                    else:
                                        command.append("-pix_fmt yuv420p")
                                    command.append(encoder_cfg_list[0]['fps_token'] + str(float(seq_list[seq_table_index]['fps_num'])/float(seq_list[seq_table_index]['fps_denom'])))
                                    command.append("-f rawvideo")
                                command.append(encoder_cfg_list[0]['input_token'] + os.path.join(clip_location, clip_))

                                if not rate_control: command.append(encoder_cfg_list[0]['qp_value_token'] + str(rc_value))
                                else:command.append(encoder_cfg_list[0]['tbr_token'] + str(rc_value))
                                if passes == 2:
                                    print("2 pass commands generation for ffmpeg not supported yet")

                                command.append(encoder_cfg_list[0]['enc_mode_token'] + str(enc_mode))
                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                #command.append(encoder_cfg_list[0]['intra_period_token'] + str(intra_period))
                                command.append(encoder_cfg_list[0]['threads_token'] + "1")
                                #command.append(encoder_cfg_list[0]['lad_token'] + "16")
                                command.append(" -c:v libsvtav1")
                                command.append(" -f ivf ")
                                if lp_number == 1:
                                    command.append(" -svtav1-params lp=1 ")
                                command.append(output_string + ".bin )")
                                command.append("> " + output_string + ".txt" + " 2>&1")
                            elif ENCODER == "ffmpeg-x264":
                                command.append("(/usr/bin/time --verbose ./ffmpeg -y")
                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]['input_res_token'] + "{}x{}".format(str(seq_list[seq_table_index]['width']), str(seq_list[seq_table_index]['height'])))
                                    if int(seq_list[seq_table_index]['bitdepth']) == 10:
                                        command.append(encoder_cfg_list[0]['bitdepth_token'] + "yuv420p10le")
                                    else:
                                        command.append("-pix_fmt yuv420p")
                                    command.append(encoder_cfg_list[0]['fps_token'] + str(float(seq_list[seq_table_index]['fps_num'])/float(seq_list[seq_table_index]['fps_denom'])))
                                    command.append("-f rawvideo")
                                command.append(encoder_cfg_list[0]['input_token'] + os.path.join(clip_location, clip_))

                                if passes == 2:
                                    print("2 pass commands generation for ffmpeg not supported yet")

                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                x264_params = []
                                #command.append(encoder_cfg_list[0]["stats_token"]+output_string+".stat")

                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                    command.append(encoder_cfg_list[0]["intra_period_min_token"] + ' ' + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                    command.append(encoder_cfg_list[0]["intra_period_min_token"] + ' ' + str(intra_period))

                                if live_encoding:
                                    if rate_control == 2:

                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["bufsize_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["maxrate_token"] + " " + str(rc_value))

                                    elif rate_control == 1:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " +str(rc_value))

                                else:
                                    if rate_control == 2:

                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["bufsize_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["maxrate_token"] + " " + str(rc_value))

                                    elif rate_control == 1:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " +str(rc_value))
                                    elif rate_control == 0:
                                        command.append(encoder_cfg_list[0]["qp_value_token"] + str(rc_value))

                                #command.append(encoder_cfg_list[0]['ffmpeg_threads_token'] + " 1")
                                command.append(" -c:v libx264")
                                if lp_number !=0:command.append(encoder_cfg_list[0]['threads_token'] + " " +str(lp_number))
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + str(enc_mode))
                                command.append(encoder_cfg_list[0]["tune_token"] + "psnr")

                                if no_scenecut != -1:
                                    x264_params.append(encoder_cfg_list[0]['no_scenecut_token'])

                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                x264_params_ = ":".join(x264_params)
                                command.append(encoder_cfg_list[0]["x264-params_token"] + " " + x264_params_)

                                command.append(" -f mp4 ")
                                command.append(output_string + ".bin )")
                                command.append("> " + output_string + ".txt" + " 2>&1")
                            elif ENCODER == "ffmpeg-x265":
                                command.append("(/usr/bin/time --verbose ./ffmpeg -y")
                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]['input_res_token'] + "{}x{}".format(str(seq_list[seq_table_index]['width']), str(seq_list[seq_table_index]['height'])))
                                    if int(seq_list[seq_table_index]['bitdepth']) == 10:
                                        command.append(encoder_cfg_list[0]['bitdepth_token'] + "yuv420p10le")
                                    else:
                                        command.append("-pix_fmt yuv420p")
                                    command.append(encoder_cfg_list[0]['fps_token'] + str(float(seq_list[seq_table_index]['fps_num'])/float(seq_list[seq_table_index]['fps_denom'])))
                                    command.append("-f rawvideo")
                                command.append(encoder_cfg_list[0]['input_token'] + os.path.join(clip_location, clip_))

                                if passes == 2:
                                    print("2 pass commands generation for ffmpeg not supported yet")

                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                x265_params = []
                                #command.append(encoder_cfg_list[0]["stats_token"]+output_string+".stat")

                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                    command.append(encoder_cfg_list[0]["intra_period_min_token"] + " " + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                    command.append(encoder_cfg_list[0]["intra_period_min_token"] + " " + str(intra_period))

                                if live_encoding:
                                    if rate_control == 2:

                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["bufsize_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["maxrate_token"] + " " + str(rc_value))

                                    elif rate_control == 1:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " +str(rc_value))

                                else:
                                    if rate_control == 2:

                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["bufsize_token"] + " " + str(rc_value))
                                        command.append(encoder_cfg_list[0]["maxrate_token"] + " " + str(rc_value))

                                    elif rate_control == 1:
                                        command.append(encoder_cfg_list[0]["bitrate_token"] + " " +str(rc_value))
                                    elif rate_control == 0:
                                        command.append(encoder_cfg_list[0]["qp_value_token"] + str(rc_value))

                                if lp_number !=0:command.append(encoder_cfg_list[0]['ffmpeg_threads_token'] + " 1")
                                command.append(" -c:v libx265")
                                if lp_number !=0: x265_params.append(encoder_cfg_list[0]['pools_token'] +str(lp_number))
                                if not live_encoding: x265_params.append(encoder_cfg_list[0]["no_wpp_token"])

                                command.append(encoder_cfg_list[0]['enc_mode_token'] + str(enc_mode))
                                command.append(encoder_cfg_list[0]["tune_token"] + "psnr")

                                if no_scenecut != -1:
                                    x265_params.append(encoder_cfg_list[0]['no_scenecut_token'])

                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                x265_params_ = ":".join(x265_params)
                                command.append(encoder_cfg_list[0]["x265-params_token"] + " " + x265_params_)

                                command.append(" -f hevc ")
                                command.append(output_string + ".bin )")
                                command.append("> " + output_string + ".txt" + " 2>&1")
                            elif ENCODER == "ffmpeg-libaom":
                                command.append("(/usr/bin/time --verbose  sh -c './ffmpeg -y")
                                for pass_number in range(passes):
                                    if pass_number:
                                        command.append(" && ./ffmpeg -y")
                                    if ".yuv" in clip_:
                                        command.append(encoder_cfg_list[0]['input_res_token'] + "{}x{}".format(str(seq_list[seq_table_index]['width']), str(seq_list[seq_table_index]['height'])))
                                        if int(seq_list[seq_table_index]['bitdepth']) == 10:
                                            command.append(encoder_cfg_list[0]['bitdepth_token'] + "yuv420p10le")
                                        else:
                                            command.append("-pix_fmt yuv420p")
                                        command.append(encoder_cfg_list[0]['fps_token'] + str(float(seq_list[seq_table_index]['fps_num'])/float(seq_list[seq_table_index]['fps_denom'])))
                                        command.append("-f rawvideo")
                                    command.append(encoder_cfg_list[0]['input_token'] + os.path.join(clip_location, clip_))
                                    command.append(encoder_cfg_list[0]['crf_token'] + str(rc_value))
                                    if intra_period == -1:
                                        command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                    else:
                                        command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))

                                    command.append(" -c:v libaom-av1")
                                    command.append(encoder_cfg_list[0]['enc_mode_token'] + ' ' + str(enc_mode))
                                    command.append(encoder_cfg_list[0]['auto_alt_ref_token'] + ' ' + "1")
                                    command.append(encoder_cfg_list[0]['lag_in_frames_token'] + ' ' + "25")
                                    command.append(' -pass ' + str(pass_number+1))
                                    if passes ==2:

                                        command.append('-passlogfile %s '%os.path.join('2pass_logs',os.path.split(output_string)[-1]))

                                    command.append(encoder_cfg_list[0]['threads_token'] + "1")

                                    aom_params = []

                                    #aom_params.append(encoder_cfg_list[0]['qp_mode_token'])
                                    #aom_params.append(encoder_cfg_list[0]['qp_value_token'] + str(rc_value))
                                    # aom_params.append(encoder_cfg_list[0]['passes_token'] + str(passes))

                                    # if intra_period == -1:
                                        # command.append(' -g ' + str(number_of_frames+1))
                                    # else:
                                        # command.append(' -g ' + str(intra_period))

                                    if passes == 1 and cqp == 1:
                                        aom_params.append(encoder_cfg_list[0]['use_fixed_qp_offsets_token'] + "1")
                                        aom_params.append(encoder_cfg_list[0]['enable_tpl_model_token'] + "0")
                                        aom_params.append(encoder_cfg_list[0]['min_gf_interval_token'] + "16")
                                        aom_params.append(encoder_cfg_list[0]['max_gf_interval_token'] + "16")
                                        aom_params.append(encoder_cfg_list[0]['gf_min_pyr_height_token'] + "4")
                                        aom_params.append(encoder_cfg_list[0]['gf_max_pyr_height_token'] + "4")
                                        command.append(encoder_cfg_list[0]['aq_mode_token'] + "0")
                                        aom_params.append(encoder_cfg_list[0]['deltaq_token'] + "0")

                                    if add_commands !='':
                                        aom_params.append(' %s ' % add_commands)

                                    # aom_params_ = ":".join(aom_params)
                                    # command.append(encoder_cfg_list[0]['aom_params_token'] + aom_params_)

                                    if pass_number == 0 and passes == 2:
                                        command.append(" -f null - ")
                                    else:
                                        command.append(" -f ivf ")
                                        command.append(output_string + ".bin")

                                command.append("' ) > " + output_string + ".txt" + " 2>&1")
                                # print('command',command)
                                # print('os.path.join(clip_location, clip_)',os.path.join(clip_location, clip_))
                                # print('\n')
                            elif ENCODER == "ffmpeg-libvpx":
                                command.append("(/usr/bin/time --verbose ./ffmpeg -y")
                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]['input_res_token'] + "{}x{}".format(str(seq_list[seq_table_index]['width']), str(seq_list[seq_table_index]['height'])))
                                    if int(seq_list[seq_table_index]['bitdepth']) == 10:
                                        command.append(encoder_cfg_list[0]['bitdepth_token'] + "yuv420p10le")
                                    else:
                                        command.append("-pix_fmt yuv420p")
                                    command.append(encoder_cfg_list[0]['fps_token'] + str(float(seq_list[seq_table_index]['fps_num'])/float(seq_list[seq_table_index]['fps_denom'])))
                                    command.append("-f rawvideo")
                                command.append(encoder_cfg_list[0]['input_token'] + os.path.join(clip_location, clip_))
                                # command.append(encoder_cfg_list[0]['qp_value_token'] + str(rc_value))
                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + " " + str(number_of_frames+1))
                                    command.append(encoder_cfg_list[0]["intra_period_min_token"] + " " + str(number_of_frames+1))

                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + " " + str(intra_period))
                                    command.append(encoder_cfg_list[0]["intra_period_min_token"] + " " + str(intra_period))

                                command.append(" -c:v libvpx-vp9")
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + " " + str(enc_mode))
                                command.append(encoder_cfg_list[0]["lag_in_frames_token"] + " " + "25")
                                command.append(encoder_cfg_list[0]["auto_alt_ref_token"] + " " + "6")
                                command.append(encoder_cfg_list[0]["threads_token"] + " " + str(lp_number))
                                command.append(encoder_cfg_list[0]["bitrate_token"] + " " + str(0))
                                command.append(encoder_cfg_list[0]["qp_value_token"] + " " + str(rc_value))

                                command.append(" -f ivf ")
                                command.append(output_string + ".bin )")
                                command.append("> " + output_string + ".txt" + " 2>&1")

                            else :

                                encoder_name = "./TAppEncoder"
                                command.append("(/usr/bin/time --verbose   "+encoder_name)
                                command.append(encoder_cfg_list[0]['config_token'] + " " + str(config_used))
                                command.append(encoder_cfg_list[0]['width_token'] + " " + str(seq_list[seq_table_index]['width']))
                                command.append(encoder_cfg_list[0]['height_token'] + " " + str(seq_list[seq_table_index]['height']))

                                command.append(encoder_cfg_list[0]['qp_value_token'] + " " + str(str(rc_value)))
                                command.append(encoder_cfg_list[0]['fps'] + " " + str(float((seq_list[seq_table_index]['fps_num'])/(seq_list[seq_table_index]['fps_denom']))))
                                if not scene_cut_clip == 1:
                                    command.append(encoder_cfg_list[0]['num_frames_token'] + " " + str(number_of_frames))
                                if intra_period == -1:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(number_of_frames+1))
                                else:
                                    command.append(encoder_cfg_list[0]["intra_period_token"] + str(intra_period))
                                if add_commands !='':
                                    command.append(' %s ' % add_commands)

                                #command.append(encoder_cfg_list[0]['intra_period_token'] + " " + str(intra_period))
                                command.append(encoder_cfg_list[0]['input_token'] + " " + os.path.join(clip_location,clip_))
                                command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin ) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")

###############################################################################################################################################################################################################################################
###################################################################################Bins are decoded and results collected below########################################################################################################################################################
###########################################################################################################################################################################################################################################

                            tmp_file = '/dev/shm/%s' % output_string

                            ffmpeg_string      = "tools/ffmpeg"
                            # model_path         = "tools/model/"
                            clean_up_yuv_command = "rm " + tmp_file + ".yuv"
                            vvenc_decoder = "./vvdecapp"

                            vvenc_decode_string = (vvenc_decoder \
                                                + " -b " + output_string +".bin"\
                                                + " -o " + tmp_file +".yuv")
                            if "10bit" in output_string:
                                pix_fmt = "yuv420p10le"
                                bit_depth_vmaf = '10'
                            else:
                                pix_fmt = "yuv420p"
                                bit_depth_vmaf = '8'
                            if clip_.endswith('yuv'):
                                yuv_resolution = str(seq_list[seq_table_index]['width']) + "x" + str(seq_list[seq_table_index]['height'])
                            if not rescale_bdrate:
                                if downscale_commands:###Vmaf extraction only supports 8bit 1080p comparisons atm
                                    match = re.search(r"_\d+x\d+to\d+x\d+_{}".format(downscale_algo[:4]),clip_)
                                    resolution = ""
                                    if match is None:
                                        match = re.search(r"_\d+x\d+",clip_)
                                        match_start = match.start()
                                        match_end = match.end()
                                        resolution = clip_[match_start+1:match_end]
                                        ref_res = resolution
                                    else:
                                        match_start = match.start()
                                        match_end = match.end()
                                        resolution = clip_[match_start+1:match_end-5].split("to")[1]
                                        ref_res = clip_[match_start+1:match_end-5].split("to")[0]
                                if enable_vmaf_threading:
                                    threads_line = '--threads %s' % number_of_threads
                                else:
                                    threads_line = ''
                                
                                if metrics_computation == 2: #No scaling one
                                    if ENCODER == 'vvenc':
                                        metrics_command_to_write = '''(./vvdecapp -b %s.bin -o %s.yuv -t 1 && tools/vmaf --reference %s --distorted %s.yuv -w %s -h %s -p 420 --aom_ctc v1.0 -b %s %s -o %s.xml && rm %s.yuv) > %s.log 2>&1 '''\
                                        % (output_string, tmp_file, os.path.join(clip_location, clip_),tmp_file, width, height,bit_depth_vmaf, threads_line, output_string, tmp_file,output_string)
                                    else:
                                        if '.y4m' in clip_:
                                            metrics_command_to_write = '''(tools/ffmpeg  -i %s.bin -strict -1 /%s.y4m && tools/vmaf --reference %s --distorted %s.y4m --aom_ctc v1.0 %s --output %s.xml && rm %s.y4m)\
> %s.log 2>&1''' % (output_string,tmp_file, os.path.join(clip_location, clip_),tmp_file,threads_line, output_string,tmp_file,output_string)
                                        elif clip_.endswith('yuv'):
                                            metrics_command_to_write = '''(tools/ffmpeg -i %s.bin -strict -1 %s.yuv && tools/vmaf --reference %s --distorted %s.yuv -w %s -h %s -p 420 --aom_ctc v1.0 -b %s %s --output %s.xml && rm %s.yuv)\
> %s.log 2>&1''' % (output_string,tmp_file, os.path.join(clip_location, clip_),tmp_file, width, height,bit_depth_vmaf,threads_line,output_string,tmp_file,output_string)
                                           
                                    ffmpeg_vmaf_file[ENC_MODES.index(enc_mode)].write(metrics_command_to_write + '\n')
                                
                                elif metrics_computation == 0:
                                    if clip_.endswith('yuv'):
                                        if ENCODER not in ['vvenc']:
                                            metrics_command_to_write = (ffmpeg_string \
                                                                            + " -y -nostdin " \
                                                                            + " -r 25" \
                                                                            + " -i " + output_string+ ".bin" \
                                                                            + " -s " + yuv_resolution \
                                                                            + " -f rawvideo -pix_fmt "+ pix_fmt \
                                                                            + " -r 25 -i " + os.path.join(clip_location, clip_) \
                                                                            + r" -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)\
                                                                            + " -f null -  > " + output_string + ".log 2>&1")
                                        elif ENCODER == "vvenc":
                                            vvenc_metrics_command_to_write = (ffmpeg_string \
                                            + " -y -nostdin" \
                                            + " -r 25" \
                                            + " -f rawvideo" \
                                            + " -pix_fmt "+ pix_fmt \
                                            + " -s:v " + yuv_resolution \
                                            + " -i " + output_string+ ".yuv" \
                                            + " -f rawvideo" \
                                            + " -r 25 -pix_fmt "+ pix_fmt \
                                            + " -s:v " + yuv_resolution \
                                            + " -i " + os.path.join(clip_location, clip_) \
                                            + r" -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)\
                                            + " -f null - " \
                                            + " > " + output_string + ".log 2>&1")

                                            clean_up_yuv_command = "rm " + tmp_file + ".yuv"

                                            metrics_command_to_write = vvenc_decode_string + " && " + vvenc_metrics_command_to_write + " && " + clean_up_yuv_command
                                            
                                    elif clip_.endswith('y4m'):
                                        if ENCODER not in ['vvenc']:
                                            metrics_command_to_write = ("tools/ffmpeg" \
                                                            + " -r 25 -i " + output_string + ".bin" \
                                                            + " -r 25 -i " + os.path.join(clip_location, clip_) \
                                                            + r" -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)\
                                                            + " -f null -  > " + output_string + ".log 2>&1")
                                    #if get_decode_cycles in [0,1]:
                                    ffmpeg_vmaf_file[ENC_MODES.index(enc_mode)].write(metrics_command_to_write + '\n')


                            if psnr_ssim_generation == 1:
                                if clip_.endswith(".y4m"):
                                    txt_file_write = "tools/ffmpeg -r 25  -i " + output_string + ".bin" + " -r 25 -i " + os.path.join(clip_location, clip_) \
                                        + " -lavfi \"ssim=stats_file=" + output_string + ".ssim;[0:v][1:v]psnr=stats_file=" + output_string + ".psnr\""  \
                                        " -f null - > " + output_string + ".log 2>&1"
                                elif clip_.endswith(".yuv"):

                                    if ENCODER not in ['vvenc']:
                                        txt_file_write = ("tools/ffmpeg" \
                                                        + " -r 25 -i " + output_string + ".bin" \
                                                        + " -s " + yuv_resolution \
                                                        + " -f rawvideo -pix_fmt "+ pix_fmt \
                                                        + " -r 25 -i " + os.path.join(clip_location, clip_) \
                                                        + " -lavfi " + "\"" + "ssim=""stats_file=" \
                                                        + output_string + ".ssim" + ";[0:v][1:v]""psnr=""stats_file=" + output_string \
                                                        + ".psnr\" -f null -  > " + output_string + ".log 2>&1")

                                    elif ENCODER == "vvenc" :
                                        vvenc_metrics_command_to_write = (ffmpeg_string \
                                        + " -y -nostdin " \
                                        + " -r 25" \
                                        + " -f rawvideo" \
                                        + " -pix_fmt "+ pix_fmt \
                                        + " -s:v " + yuv_resolution \
                                        + " -i " + output_string+ ".yuv" \
                                        + " -f rawvideo" \
                                        + " -r 25 -pix_fmt "+ pix_fmt \
                                        + " -s:v " + yuv_resolution \
                                        + " -i " + os.path.join(clip_location, clip_) \
                                        + " -lavfi \"ssim=stats_file=" + output_string + ".ssim;[0:v][1:v]psnr=stats_file=" + output_string + ".psnr\""  \
                                        + " -f null - " \
                                        + " > " + output_string + ".log 2>&1")

                                        txt_file_write = vvenc_decode_string + " && " + vvenc_metrics_command_to_write + " && " + clean_up_yuv_command


                                ffmpeg_file[ENC_MODES.index(enc_mode)].write(txt_file_write + '\n')


                            if vmaf_generation == 1:

                                if ".yuv" in clip_ and ENCODER == 'vvenc':
                                    vvenc_metrics_command_to_write = (ffmpeg_string \
                                    + " -y -nostdin " \
                                    + " -r 25" \
                                    + " -f rawvideo" \
                                    + " -pix_fmt "+ pix_fmt \
                                    + " -s:v " + yuv_resolution \
                                    + " -i " + output_string+ ".yuv" \
                                    + " -f rawvideo" \
                                    + " -r 25 -pix_fmt "+ pix_fmt \
                                    + " -s:v " + yuv_resolution \
                                    + " -i " + os.path.join(clip_location, clip_) \
                                    + r" -lavfi '[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf:log_path={}.vmaf:log_fmt=xml'".format(output_string)
                                    + " -f null - " \
                                    + " > " + output_string + ".vmaf_log 2>&1")

                                    vmaf_txt_file_write = vvenc_decode_string + " && " + vvenc_metrics_command_to_write + " && " + clean_up_yuv_command

                                elif ".yuv" in clip_:
                                    vmaf_txt_file_write = (ffmpeg_string \
                                                        + " -y -nostdin " \
                                                        + " -r 25" \
                                                        + " -i " + output_string+ ".bin" \
                                                        + " -f rawvideo" \
                                                        + " -r 25 -pix_fmt "+ pix_fmt \
                                                        + " -s:v " + "{}x{}".format(width,height) \
                                                        + " -i " + os.path.join(clip_location, clip_) \
                                                        + r" -lavfi '[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf:log_path={}.xml:log_fmt=xml'".format(output_string)
                                                        + " -f null - > " + output_string + ".vmaf_log 2>&1")
                                elif ".y4m" in clip_:
                                    vmaf_txt_file_write = (ffmpeg_string \
                                                        + " -y -nostdin " \
                                                        + " -i " + output_string+ ".bin" \
                                                        + " -i " + os.path.join(clip_location, clip_) \
                                                        + r" -lavfi '[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf:log_path={}.vmaf:log_fmt=xml'".format(output_string)
                                                        + " -f null - > " + output_string + ".vmaf_log 2>&1")


                                vmaf_file[ENC_MODES.index(enc_mode)].write(vmaf_txt_file_write + '\n')

                            if rescale_bdrate == 1:

                                match = re.search(r"_(\d+x\d+to\d+x\d+)_{}_".format(downscale_algo[:4]),clip_)
                                resolution = ""
                                if match is None:
                                    match = re.search(r"_(\d+x\d+)_?",clip_)
                                    resolution = match.group(1)
                                    ref_res = resolution
                                    ref_clip_name = clip_
                                else:
                                    resolution = match.group(1).split("to")[1]
                                    ref_res = match.group(1).split("to")[0]
                                    ref_clip_name = clip_.replace('to%s'%resolution,'').replace('_%s'%downscale_algo[:4],'')
                                    if '_.y4m' in ref_clip_name or '_.yuv' in ref_clip_name:
                                        ref_clip_name = ref_clip_name.replace('_.y4m','.y4m').replace('_.yuv','.yuv')
                                if metrics_computation == 2:

                                    if ENCODER == 'vvenc':
                                        if yuv_resolution != ref_res:
                                            metrics_command_to_write = '''(./vvdecapp -b %s.bin -o %s_dec.yuv -t 1 && tools/ffmpeg -s:v %s  -f rawvideo -i %s_dec.yuv -s:v %s -f rawvideo -i %s -lavfi "scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5 [scaled][ref]" \
-map "[ref]" -f null - -map "[scaled]"  -f rawvideo %s.yuv && rm %s_dec.yuv && tools/vmaf --reference %s --distorted %s.yuv -w %s -h %s -p 420 --aom_ctc v1.0 -b %s -o %s.xml && rm %s.yuv) > %s.log 2>&1 '''\
                                            % (output_string, tmp_file, yuv_resolution, tmp_file, ref_res, os.path.join(clip_location, ref_clip_name), tmp_file, tmp_file, os.path.join(clip_location, ref_clip_name),tmp_file,ref_res.split('x')[0], ref_res.split('x')[1],bit_depth_vmaf, output_string,tmp_file,output_string)
                                        else:
                                            metrics_command_to_write = '''(./vvdecapp -b %s.bin -o %s.yuv -t 1 && tools/vmaf --reference %s --distorted %s.yuv -w %s -h %s -p 420 --aom_ctc v1.0 -b %s -o %s.xml && rm %s.yuv) > %s.log 2>&1 '''\
                                            % (output_string, tmp_file, os.path.join(clip_location, ref_clip_name),tmp_file,width, height,bit_depth_vmaf, output_string,tmp_file,output_string)
                                    else:
                                        if '.y4m' in clip_:
                                            metrics_command_to_write = '''(tools/ffmpeg -i %s.bin -i %s -lavfi "scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5 [scaled][ref]" -map "[ref]" -f null - -map "[scaled]" \
-strict -1 -pix_fmt %s %s.y4m && tools/vmaf --reference %s --distorted %s.y4m --aom_ctc v1.0 --output %s.xml && rm %s.y4m)\
> %s.log 2>&1''' % (output_string, os.path.join(clip_location, ref_clip_name),pix_fmt,tmp_file, os.path.join(clip_location, ref_clip_name),tmp_file,output_string,tmp_file,output_string)
                                        elif clip_.endswith('yuv') or encoder in ['x265','ffmpeg-x265']:
                                            metrics_command_to_write = '''(tools/ffmpeg -i %s.bin -s:v %s -f rawvideo -i %s  -lavfi "scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5 [scaled][ref]" -map "[ref]" -f null - -map "[scaled]" \
-strict -1 -pix_fmt %s %s.yuv && tools/vmaf --reference %s --distorted %s.yuv -w %s -h %s -p 420 --aom_ctc v1.0 -b %s --output %s.xml && rm %s.yuv)\
> %s.log 2>&1''' % (output_string,ref_res, os.path.join(clip_location, ref_clip_name),pix_fmt,tmp_file, os.path.join(clip_location, ref_clip_name),tmp_file, ref_res.split('x')[0], ref_res.split('x')[1],bit_depth_vmaf, output_string,tmp_file,output_string)

                                    #ffmpeg_vmaf_file[ENC_MODES.index(enc_mode)].write(txt_file_write + '\n')

                                if ".yuv" in clip_:
                                    if metrics_computation == 1 and ENCODER not in ['vvenc']:
                                        metrics_command_to_write = (ffmpeg_string \
                                                                    + " -y -nostdin " \
                                                                    + " -r 25" \
                                                                    + " -i " + output_string+ ".bin" \
                                                                    + " -f rawvideo" \
                                                                    + " -r 25 -pix_fmt "+ pix_fmt \
                                                                    + " -s:v " + ref_res \
                                                                    + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                    + " -lavfi \'scale2ref=flags={}+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; ".format(downscale_algo) \
                                                                    + "[scaled1][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                    + "[scaled2][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                                                    + "[scaled3][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf:log_path={}:log_fmt=xml' -map \"[ref]\" -f null -".format(output_string + ".vmaf") \
                                                                    + " > " + output_string + ".log 2>&1")
                                    elif metrics_computation == 0 and ENCODER not in ['vvenc']:
                                        if ref_res == resolution:
                                            metrics_command_to_write = (ffmpeg_string \
                                                                        + " -y -nostdin " \
                                                                        + " -r 25" \
                                                                        + " -i " + output_string+ ".bin" \
                                                                        + " -f rawvideo" \
                                                                        + " -r 25 -pix_fmt "+ pix_fmt \
                                                                        + " -s:v " + ref_res \
                                                                        + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                        + r" -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)\
                                                                        + " -f null - > " + output_string + ".log 2>&1")
                                        else:
                                            metrics_command_to_write = (ffmpeg_string \
                                                                        + " -y -nostdin " \
                                                                        + " -r 25" \
                                                                        + " -i " + output_string+ ".bin" \
                                                                        + " -f rawvideo" \
                                                                        + " -r 25 -pix_fmt "+ pix_fmt \
                                                                        + " -s:v " + ref_res \
                                                                        + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                        + " -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=1 [scaled1];" \
                                                                        + r" [scaled1][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)\
                                                                        + " -map \"[ref]\" -f null - > " + output_string + ".log 2>&1")
                                    elif ENCODER == "vvenc" and metrics_computation == 0:
                                        vvenc_decoder = "./vvdecapp"
                                        if ref_res == resolution:
                                            vvenc_metrics_command_to_write = (ffmpeg_string \
                                            + " -y -nostdin " \
                                            + " -r 25" \
                                            + " -f rawvideo" \
                                            + " -pix_fmt "+ pix_fmt \
                                            + " -s:v " + resolution \
                                            + " -i " + output_string+ ".yuv" \
                                            + " -f rawvideo" \
                                            + " -r 25 -pix_fmt "+ pix_fmt \
                                            + " -s:v " + ref_res \
                                            + " -i " + os.path.join(clip_location, ref_clip_name) \
                                            + r" -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)\
                                            + " -f null - > " + output_string + ".log 2>&1")


                                        else:
                                            vvenc_metrics_command_to_write = (ffmpeg_string \
                                            + " -y -nostdin " \
                                            + " -r 25" \
                                            + " -f rawvideo" \
                                            + " -pix_fmt "+ pix_fmt \
                                            + " -s:v " + resolution \
                                            + " -i " + output_string+ ".yuv" \
                                            + " -f rawvideo" \
                                            + " -r 25 -pix_fmt "+ pix_fmt \
                                            + " -s:v " + ref_res \
                                            + " -i " + os.path.join(clip_location, ref_clip_name) \
                                            + " -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=1 [scaled1];" \
                                            + r"[scaled1][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)\
                                            + " -map \"[ref]\" -f null - > " + output_string + ".log 2>&1")

                                        metrics_command_to_write = vvenc_decode_string + " && " + vvenc_metrics_command_to_write + " && " + clean_up_yuv_command

                                    elif ENCODER == "vvenc" and metrics_computation == 1:
                                        vvenc_decoder = "./vvdecapp"
                                        vvenc_metrics_command_to_write = (ffmpeg_string \
                                        + " -y -nostdin " \
                                        + " -r 25" \
                                        + " -f rawvideo" \
                                        + " -pix_fmt "+ pix_fmt \
                                        + " -s:v " + resolution \
                                        + " -i " + output_string+ ".yuv" \
                                        + " -f rawvideo" \
                                        + " -r 25 -pix_fmt "+ pix_fmt \
                                        + " -s:v " + ref_res \
                                        + " -i " + os.path.join(clip_location, ref_clip_name) \
                                        + " -lavfi \'scale2ref=flags={}+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; ".format(downscale_algo) \
                                        + "[scaled1][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                        + "[scaled2][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                        + "[scaled3][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf:log_path={}:log_fmt=xml' -map \"[ref]\" -f null -".format(output_string + ".vmaf") \
                                        + " > " + output_string + ".log 2>&1")

                                        metrics_command_to_write = vvenc_decode_string + " && " + vvenc_metrics_command_to_write + " && " + clean_up_yuv_command


                                      
                                elif ".y4m" in clip_:
                                
                                    if metrics_computation == 1:
                                        metrics_command_to_write = (ffmpeg_string \
                                                                    + " -y -nostdin " \
                                                                    + " -r 25 " \
                                                                    + " -i " + output_string+ ".bin" \
                                                                    + " -r 25 " \
                                                                    + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                    + " -lavfi \'scale2ref=flags={}+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; ".format(downscale_algo) \
                                                                    + "[scaled1][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                    + "[scaled2][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                                                    + "[scaled3][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf:log_path={}:log_fmt=xml' -map \"[ref]\" -f null -".format(output_string + ".vmaf") \
                                                                    + " > " + output_string + ".log 2>&1")
                                    elif metrics_computation == 0:
                                        if ref_res == resolution:
                                            metrics_command_to_write = (ffmpeg_string \
                                                                        + " -y -nostdin " \
                                                                        + " -r 25 " \
                                                                        + " -i " + output_string+ ".bin" \
                                                                        + " -r 25 " \
                                                                        + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                        + r" -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)
                                                                        + " -f null - > " + output_string + ".log 2>&1")
                                        else:
                                            metrics_command_to_write = (ffmpeg_string \
                                                                        + " -y -nostdin " \
                                                                        + " -r 25 " \
                                                                        + " -i " + output_string+ ".bin" \
                                                                        + " -r 25 " \
                                                                        + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                        + " -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=1 [scaled1];" \
                                                                        + " [scaled1][1:v]libvmaf=aom_ctc=1:log_path={}.xml:log_fmt=xml'".format(output_string)
                                                                        + " -map \"[ref]\" -f null - > " + output_string + ".log 2>&1")
                                if metrics_computation not in [0,2]:
                                    rescale_metrics_file[ENC_MODES.index(enc_mode)].write(metrics_command_to_write + '\n')
                                else:
                                    ffmpeg_vmaf_file[ENC_MODES.index(enc_mode)].write(metrics_command_to_write + '\n')




                            command.append("\n")
                            command_ = " ".join(command)
                            fo[ENC_MODES.index(enc_mode)].write(command_)
                            if second_command:
                                second_command.append("\n")
                                second_command_.append(" ".join(second_command))
                                fo[ENC_MODES.index(enc_mode)].write(second_command_)

        fo[ENC_MODES.index(enc_mode)].write('\n')
        fo[ENC_MODES.index(enc_mode)].write('\n')

    for enc_mode in ENC_MODES:
        fo[ENC_MODES.index(enc_mode)].close()
        if psnr_ssim_generation == 1:
            ffmpeg_file[ENC_MODES.index(enc_mode)].close()
        if vmaf_generation ==1 :
            vmaf_file[ENC_MODES.index(enc_mode)].close()
        if rescale_bdrate == 1 and metrics_computation not in [0,2]:
            rescale_metrics_file[ENC_MODES.index(enc_mode)].close()

generate_commands(CLIP_DIRS, clip_location, ENC_MODES, passes, cqp, psnr_ssim_generation, vmaf_generation, disable_enhancement_vmaf,
                      scene_cut_clip, scene_changes_suffix, downscale_commands, rescale_bdrate, downscale_target_resolutions,
                      bitstream_folders, ENCODER,seq_list)

#### GENERATE BASH FILE
if cqp == 1:
    encoder_test_identifier = ENCODER +"-" +str(passes)+"p-cqp"
else:
    encoder_test_identifier = ENCODER +"-" +str(passes)+"p"

run_all_paral_file_name = encoder_test_identifier + '-run-all-paral.sh'
run_paral_cpu_file_name = encoder_test_identifier + '-run-paral-cpu.sh'
generate_bash_driver_file(num_pool, ffmpeg_job_count, ENCODER, run_all_paral_file_name, run_paral_cpu_file_name, ENC_MODES, cqp, scene_cut_clip, bitstream_folders)
generate_parameters_file()
