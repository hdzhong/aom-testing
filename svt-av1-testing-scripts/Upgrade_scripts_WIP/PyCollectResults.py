import os, sys, glob, re, csv
import multiprocessing, subprocess
from datetime import date
import time

line_template = '{codec}\t{enc_name}\t{width}x{height}\t{bit_depth}\t{input_sequence}\t{qp}\t{file_size}\t{psnr_y}\t{psnr_u}\t{psnr_v}\t{psnr_all}\t{ssim_y}\t{ssim_u}\t{ssim_v}\t{ssim_all}\t{vmaf}\t{vmaf_neg}\t{cpu_usage}\t{avg_speed}\t{avg_latency}\t{frame_count}\t{encode_user_time}\t{encode_wall_time}\t{max_memory}\t{encode_sys_time}\t{decode_sys_time}\t{decode_user_time}\t{bitrate}'

'''CVH variables'''
cvh_metrics = ['PSNR_Y',"SSIM_Y",'VMAF','VMAF_NEG']
collect_cvh = 0
max_number_of_cvh_points = 40

'''System Variables'''
number_of_cores = multiprocessing.cpu_count()

cwd = os.getcwd()
test_dir = os.path.join(cwd,'bitstreams')
# test_dir = r'\\vccv01a-cifs.vc.intel.com\emblaze_testing_1\SPIE2021\Final-logs-bins\UGC\AOM\Bitstreams'

'''Encode log patterns'''
PATTERNS = {
    'codec' : [lambda x: x.split('[info]: using')[1].split('\n')[1].split('[info]')[0].strip(), \
                      lambda x: x.split('Codec: ')[1].split('\n')[0].split(' ')[-3], \
                      lambda x: x.split('SVT [version]:')[1].split(' ')[0]
                      ],

    'width' : [lambda x: x.split(' [info]:')[1].split('x')[0].strip(), \
                      lambda x: x.split('g_w                          = ')[1].split('\n')[0], \
                      lambda x: x.split('width / height')[1].split('\n')[0].split(': ')[1].split('/')[0].strip(), \
                      lambda x: x.split('SourceHeight')[1].split('\n')[0].split(': ')[1].split('/')[0].strip()
                      ],

    'height' : [lambda x: x.split('[info]:')[1].split('x')[1].split(' ')[0].strip().replace('p',''), \
                       lambda x: x.split('g_h                          = ')[1].split('\n')[0], \
                       lambda x: x.split('width / height')[1].split('\n')[0].split(': ')[1].split('/')[1].strip(), \
                       lambda x: x.split('SourceHeight')[1].split('\n')[0].split(': ')[1].split('/')[0].strip()
                       ],

    'bit_depth' : [lambda x: x.split('[info]: profile ')[1].split('\n')[0].split(',')[-1].split('-')[0].strip(), \
                          lambda x: x.split('[info]:')[1].split('i')[1].split(' ')[0].split('p')[1], \
                          lambda x: x.split('g_bit_depth                  = ')[1].split('\n')[0], \
                          lambda x: x.split('SVT [config]: bit-depth')[1].split(':')[1].split('/')[0].strip(), \
                          lambda x: x.split('CompressedTenBitFormat')[1].split('/')[0].split(':')[1].strip()
                          ],

    'bitrate' : [lambda x: x.split('kb/s,')[-2].split(',')[-1].strip(), \
                        lambda x: x.split('b/s')[-2].split('b/f ')[-1], \
                        lambda x: x.split('Total Frames')[1].split('kbps')[0].split('\t')[-1].strip()
                        ],

    'frame_count' : [lambda x: x.split(' frames')[-2].split('encoded')[1].strip(), \
                            lambda x: x.split(' frame ')[-1].strip().split(' ')[0].split('/')[1], \
                            lambda x: x.split('Total Frames')[1].strip().split('\n')[1].split('\t')[0].strip()
                            ],

    'commit' : [lambda x: x.split('SVT [version]:')[1].split('\n')[0].split('g')[1], \
                       lambda x: x.split('Codec: ')[1].split('\n')[0].split(' ')[-1], \
                       lambda x: x.split('Codec: AOMedia ')[1].split('\n')[0].split('-')[-1]
                       ],

    'cpu_usage' : [lambda x: x.split('Percent of CPU this job got: ')[1].split('\n')[0]],
    
    'encode_user_time' : [lambda x: x.split('User time (seconds): ')[1].split('\n')[0]],
    
    'encode_sys_time' : [lambda x: x.split('System time (seconds): ')[1].split('\n')[0]],
    
    'encode_wall_time' : [lambda x: x.split('Elapsed (wall clock) time (h:mm:ss or m:ss): ')[1].split('\n')[0]],
    
    'max_memory' : [lambda x: x.split('Maximum resident set size (kbytes): ')[1].split('\n')[0]],

    #'''Metric log Patterns'''
    'psnr_y' : [lambda x: x.split('psnr_y"')[1].split('mean="')[1].split('"')[0],lambda x: x.split('PSNR y:')[1].split(' ')[0]],
    
    'psnr_u' : [lambda x: x.split('psnr_cb"')[1].split('mean="')[1].split('"')[0],lambda x: x.split(' u:')[1].split(' ')[0]],
    
    'psnr_v' : [lambda x: x.split('psnr_cr"')[1].split('mean="')[1].split('"')[0],lambda x: x.split(' v:')[1].split(' ')[0]],
    
    'psnr_all' : [lambda x: x.split(' average:')[1].split(' ')[0]],
    
    'ssim_y' : [lambda x: x.split('float_ssim"')[1].split('mean="')[1].split('"')[0],lambda x: x.split('] SSIM Y:')[1].split(' (')[1].split(') ')[0]],
    
    'ssim_u' : [lambda x: x.split(') U:')[1].split(' (')[1].split(') ')[0]],
    
    'ssim_v' : [lambda x: x.split(') V:')[1].split(' (')[1].split(') ')[0]],
    
    'ssim_all' : [lambda x: x.split(') All:')[1].split(' (')[1].split(')')[0]],
    
    'vmaf' : [lambda x: x.split('vmaf"')[1].split('mean="')[1].split('"')[0],lambda x: x.split('VMAF score: ')[1].split('\n')[0].strip()],
    
    'vmaf_neg' : [lambda x: x.split('vmaf_neg"')[1].split('mean="')[1].split('"')[0]],

    'decode_sys_time' : [lambda x: ''],
    
    'decode_user_time' : [lambda x: ''],
    
    'avg_speed' : [lambda x: ''],
    
    'avg_latency' : [lambda x: ''],
    
    'avg_speed_line' : [lambda x: ''],
    
    'avg_latency_line' : [lambda x: ''],
    
    'svt_1st_pass_time_line' :[lambda x: ''],
    
    'frames_per_second':[lambda x: ''],
    
    'fps_line':[lambda x: ''],
    
    'Num_passes' : [lambda x: ''],

}

def main():
    bitstreams = list()
    metric_results = list()
    full_data_for_writing = list()
    bitstream_folder = test_dir
    all_presets = list()
    header = line_template.replace('{','').replace('}','').upper()

    log_files = [log for log in glob.glob('%s/*.log'%bitstream_folder) if '_vmaf' not in log]     
    t0 = time.time()

    results = execute_parallel_commands(number_of_cores, log_files, cwd, 'metrics')
    
    for metric_result,bitstream in results:

        metric_results.append(metric_result)
        bitstreams.append(bitstream)

    stream_file_sizes = execute_parallel_commands(number_of_cores, bitstreams, cwd, 'file_size')

    print('Time taken: ', time.time()-t0)
     
    for metric_result, file_size in zip(metric_results, stream_file_sizes):
        try:
            number_of_frames = int(metric_result.split('\t')[20])
        except:
            print(metric_result.split('\t'))
            
        codec = metric_result.split('\t')[0]
        preset = metric_result.split('\t')[1].split('_')[-1]
        enc_name = '_'.join(metric_result.split('\t')[1].split('_')[:-1])
        all_presets.append(preset)
        if codec not in ['x264', 'x265']:
            file_size=file_size-(32+(12*number_of_frames))
            
        line_to_write = metric_result.format(**vars())    
        full_data_for_writing.append(line_to_write)
        
    sorted_data_for_writing=sorted([x.split('\t') for x in full_data_for_writing], key=lambda x: (x[1],x[4],x[2],x[5]))
    
    sorted_data_for_writing.insert(0,header.split('\t'))
    all_presets = sorted(list(set(all_presets)))
    print('list(all_presets)',list(all_presets))
    
    '''Merge in filesize data and write to output file'''
    with open('_result_{}_{}.txt'.format(enc_name,'%s-%s'%(all_presets[0],all_presets[-1])),'w') as out:
        for data in sorted_data_for_writing:
            data = '\t'.join(data)
            out.write(data)
            out.write('\n')
    if collect_cvh:
        get_cvh_results(['\t'.join(x) for x in sorted_data_for_writing],all_presets)


def get_metrics(file):
    RESULT = dict()
    data_to_search = list()
    root_file = os.path.splitext(file)[0]

    prefixes_to_search = ['xml','txt','log','vmaf_log']
    
    '''prepare the nested list of data to be searched through by check_for_metric'''
    for prefix in prefixes_to_search:
        file_to_search = '%s.%s' % (root_file, prefix)
        
        if os.path.exists(file_to_search):
            with open(file_to_search) as log:
                log_content = log.read()
                data_to_search.append(log_content)
                
    '''Grab all metrics in the pattern dict'''
    for metric in PATTERNS:
        RESULT[metric] = check_for_metric(data_to_search, PATTERNS[metric]).strip('\t')

    '''Get following metrics from filename'''
    file_name = os.path.split(file)[-1]
    
    RESULT['input_sequence'] = re.search(r'M\d+_*(.*?)_*Q\d+.*?',file_name).group(1)

    if re.search(r'\d+x\d+to\d+x\d+',RESULT['input_sequence']):
        RESULT['sequence_resolution'] = re.search(r'\d+x\d+to\d+x\d+',RESULT['input_sequence']).group(0)
    elif re.search(r'\d+x\d+',RESULT['input_sequence']):
        RESULT['sequence_resolution'] = re.search(r'\d+x\d+',RESULT['input_sequence']).group(0)
    else:
        RESULT['sequence_resolution'] = ''

    '''Refining section'''
    if RESULT['encode_wall_time'] == 'n/a':
        print('file',file)

    if RESULT['encode_wall_time'].count(":") == 1:
        m, s = RESULT['encode_wall_time'].split(":")
        RESULT['encode_wall_time'] = float(m)*60 + float(s)
    else:
        h, m, s = RESULT['encode_wall_time'].split(":")
        RESULT['encode_wall_time'] = float(h)*3600 + float(m)*60 + float(s)
        
    if RESULT['codec'] in ['AV1','VP9']:
        RESULT['bitrate'] = float(RESULT['bitrate'])/1000
    '''Path for elfuente, push resolution ot end to avoid self similar clip issue'''
    RESULT['input_sequence'] = RESULT['input_sequence'].replace(RESULT['sequence_resolution'],'').replace('_lanc','') + '_' + RESULT['sequence_resolution']
    
    RESULT['collection_date'] = date.today().strftime("%Y-%m-%d")
    RESULT['file_size'] = '{file_size}'
    RESULT['bit_depth'] = '{}bit'.format(RESULT['bit_depth'])
    RESULT['qp'] = re.search(r'_Q(\d+).*?',file_name).group(1)
    RESULT['preset'] = re.search(r'_M(\d+)_',file_name).group(1)
    RESULT['enc_name'] = '{codec}_{commit}_{bit_depth}_{collection_date}_M{preset}'.format(**RESULT).replace(' ','_')

    '''Convert time into ms'''
    RESULT['encode_wall_time'] = float(RESULT['encode_wall_time']) * 1000
    RESULT['encode_user_time'] = float(RESULT['encode_user_time']) * 1000
    RESULT['encode_sys_time'] = float(RESULT['encode_sys_time']) * 1000
    
    bitstream = '%s.bin' % root_file
    line_to_write = line_template.format(**RESULT)

    return line_to_write, bitstream


def check_for_metric(data_to_search, pattern_list):
    '''Order of files to search, first found first served'''

    for data in data_to_search:
        for pattern in pattern_list:
                
            content_split = 'data%s' % pattern
            try:

                metric = pattern(data)
                # print('metric',metric)
                # if pattern_list == width_patterns:   
                    # print('\n')
                    # print('metric data',content_split)#
                    # print('\n')
                return metric
            except IndexError:
                pass


    return 'n/a'


def get_metric_indices(result_lines):
    metric_indices = list()
    rate_index = ''
    qp_detected = False
    for index, line in enumerate(result_lines):
        line_split = line.split('\t')

        if index == 0:
            header = line_split
        if len(line_split) > 5 and line_split[5].strip().isdigit():
            for index,element in enumerate(line_split):
                if qp_detected and element.isdigit() and not rate_index:
                    rate_index = index
                elif qp_detected and not element.isdigit():
                    metric_indices.append(index)
                elif element.isdigit() and qp_detected:
                    break
                if element.isdigit() and not qp_detected:
                    qp_detected = True

            break

    return metric_indices, rate_index, header

    
def get_cvh_results(result_lines,all_presets):
    metric_indices = list()
    cvh_result_lines = list()  
    number_of_elements = 0

    '''Get the index of rate and a list of potential metrics to perform cvh on'''
    '''Note, Metric index gathering is greedy and may include invalid indices.'''
    '''Doesnt matter as we only gather the metric columns with reference to the header'''
    #May include for fix above in future
    if not metric_indices:
        metric_indices, rate_index, header = get_metric_indices(result_lines)

    '''Perform CVH analysis on the various metrics'''
    for metric_index in metric_indices:
        rates = list()
        metrics = list()   
        cvh_commands = list()  
        prev_sequence_name = None
        number_of_elements_tracker = [0]
        if header[metric_index] in cvh_metrics:
            cvh_metric = header[metric_index]
        else:
            continue
        
        for index, line in enumerate(result_lines):
            line_split = line.split('\t')
            
            if len(line_split) > 5 and line_split[5].strip().isdigit():
                sequence_name = line_split[4]
                root_sequence_name = re.search(r"(.*?)_\d+x\d+", sequence_name).group(1)
                rate = line_split[rate_index]
                metric = line_split[metric_index]

                if not prev_sequence_name or root_sequence_name == prev_sequence_name:
                    prev_sequence_name = root_sequence_name
                    rates.append(rate)
                    metrics.append(metric)
                    cvh_result_lines.append(line)
                    
                if root_sequence_name != prev_sequence_name or index == len(result_lines)-1:
                    number_of_elements = len(rates)
                    number_of_elements_tracker.append(number_of_elements)
                    prev_sequence_name = root_sequence_name
                    cvh_command = './tools/convex_hull_exe {0} {1} {2}'.format(str(len(rates)), ' '.join(rates), ' '.join(metrics))

                    cvh_commands.append(cvh_command)

                    rates = list()
                    metrics = list()
                    rates.append(rate)
                    metrics.append(metric)
                    
        cvh_indices = list()
        temp_cvh_results = execute_parallel_commands(number_of_cores, cvh_commands, cwd, 'cvh')

        '''Get optimal metric indices from convex hull exe'''
        index = 0
        current_line_number = 0
        for temp_cvh_result,number_of_elements in zip(temp_cvh_results,number_of_elements_tracker):
            temp_cvh_indices = list()
            current_line_number = current_line_number + number_of_elements
            for cvh_line in str(temp_cvh_result).split('\n'):
                if '=' in cvh_line:
                    cvh_index = int(cvh_line.split(',')[0].split('=')[1]) + current_line_number + 1 #(index*(number_of_elements)) +1
                    temp_cvh_indices.append(cvh_index)
            cvh_indices.append(temp_cvh_indices)
            index += 1
            
        '''Write selected convex hull selections to output and add filler lines for conformity'''
        with open('{}_convex_hull_data_result_{}_{}.txt'.format(cvh_metric,date.today().strftime("%Y-%m-%d"),'%s-%s'%(list(all_presets)[0],list(all_presets)[-1])),'w') as out:
            out.write('\t'.join(header))
            out.write('\n')
            for temp_cvh_indices in cvh_indices:
                for cvh_index in temp_cvh_indices[::-1]:
                    result_line_split = result_lines[cvh_index].split('\t')
                    
                    sequence_name = result_line_split[4]
                    root_sequence_name = re.search(r"(.*?)_\d+x\d+", sequence_name).group(1)
                    
                    '''Overwrite the encoding name and sequence name to cvh format'''
                    result_line_split[4] = root_sequence_name
                    result_line_split[1] = '%s_%s' % (result_line_split[1],cvh_metric)
                    
                    '''Overwrite the irrelevant data columns'''
                    for i in range(rate_index+1,len(header)):
                        if not result_line_split[i].isdigit() and i != metric_index:
                            result_line_split[i] = 'n/a'
                        elif result_line_split[i].isdigit():
                            break
                            
                    '''Write the resulting line'''
                    out.write('\t'.join(result_line_split))
                    out.write('\n')
                    
                if len(temp_cvh_indices) < max_number_of_cvh_points:
                    for i in range(0,max_number_of_cvh_points-len(temp_cvh_indices)):
                        filler_line =result_line_split[:rate_index-1] + ['99'] + ['n/a']*(len(result_line_split)-6)
                        out.write('%s'%'\t'.join(filler_line))
                        out.write('\n')


def execute_cvh_commands(inputs):
    cmd, work_dir = inputs

    pipe = subprocess.Popen(cmd, shell=True, cwd=work_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = pipe.communicate()
    pipe.wait()

    return output


def execute_file_size_commands(inputs):
    stream, work_dir = inputs
    
    file_size = os.path.getsize(stream)

    return file_size


def execute_parallel_commands(number_of_processes, command_list, execution_directory, execution_type):
    command_lines = [command.strip() for command in command_list]
    execution_directory_list = [execution_directory for i in enumerate(command_lines)]
    inputs = zip(command_lines, execution_directory_list)
    Pooler = multiprocessing.Pool(processes=number_of_processes, maxtasksperchild=30)
    if execution_type == 'cvh':
        output = Pooler.map(execute_cvh_commands, inputs)
    elif execution_type == 'file_size':
        output = Pooler.map(execute_file_size_commands, inputs)
    elif execution_type == 'metrics':
        output = Pooler.map(get_metrics, command_list)
    Pooler.close()
    Pooler.join()
    
    if sys.platform == 'Linux':
        os.system('stty sane')

    return output    


if __name__ == '__main__':
    main()

        
