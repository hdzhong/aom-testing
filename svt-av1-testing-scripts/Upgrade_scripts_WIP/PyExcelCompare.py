import glob
import xlwings as xw
import os
import re
import time
import sys


#path variables
result_directory = os.path.join(os.getcwd(),'results')
result_files=glob.glob('%s/*.txt' % result_directory)

#Excel Variables
wb = xw.Book('Comparison_templateV4_2022-06-13.xlsm')
master_sheet = wb.sheets['master']
##master_cvh_sheet = wb.sheets['master-cvh']
summary_sheet=wb.sheets['Summary']
all_data_sheet=wb.sheets['All_Data']

#Result Variables
BDR_HEADER = list()
SYSTEM_HEADER = list()

BDR_RESULTS=[]
SYSTEM_RESULTS=[]

master_cvh_sheet = master_sheet
grouped_encoders = dict()
def main():
    cvh_comparison = None
    '''Check the validity of results files'''
    valid_results,reason = verify_result_file_integrity(result_files)

    if valid_results:
        print('Results Integrity Check PASSED...')
    else:
        print('Results Integrity Check FAILED...')
        print(reason)
        
        response = None
        while response not in ['y','n']: 
            response = input('Do you wish to continue? (y,n) -> ')
            if response == 'y':
                break
            elif response == 'n':
                return
            
##
    wb.macro('Turn_Off_Features')()
    try:
        '''Search for the next available line to insert new results and data'''
        data_range=uniform_search(all_data_sheet)
        summary_range=uniform_search(summary_sheet)
        print('summary_range',summary_range)
        print('data_range',data_range)
        '''Populate all data page with result files found in results folder'''
        set_data(result_files,data_range)

        '''Get encoder names from all data page'''
        encoder_names_from_all_data = all_data_sheet.range("B:B").value
        sequence_names_from_all_data = all_data_sheet.range("E:E").value
        qp_values_from_all_data = all_data_sheet.range("F:F").value
        squashed_encoder_list = sorted([x for x in set(encoder_names_from_all_data) if x != None])

        '''Get user input on which Comparison they'd like to perform'''
        comparison_selection, enc_selection = get_comparison_type_selection(squashed_encoder_list)

        '''Build the lists of selected mod and ref root encoder names'''
        mod_encoder_names, ref_encoder_names = get_ref_mod_encoder_names(enc_selection, comparison_selection)

        '''Check if there exists relevant cvh data in the all data page, offer the option if true'''
        cvh_comparison, mod_metrics,ref_metrics = check_cvh_comparison_options(squashed_encoder_list, mod_encoder_names, ref_encoder_names)
        
        '''Get the number of cvh points and QPs for the encoders that are being compared'''
        number_of_cvh_points, number_of_qps = get_number_of_points(mod_metrics,ref_metrics, encoder_names_from_all_data, sequence_names_from_all_data,mod_encoder_names,ref_encoder_names, qp_values_from_all_data, cvh_comparison)

        '''Adjust the excel bdr tables according to the found number of points above'''

        adjust_excel_cvh(number_of_qps,number_of_cvh_points, 10000, cvh_comparison, master_cvh_sheet)

        '''Perform the comparisons'''
        perform_comparisons(mod_encoder_names, ref_encoder_names, mod_metrics,ref_metrics, cvh_comparison, comparison_selection)

        '''Write the generated results to the summary page'''
        set_results(summary_range, cvh_comparison, mod_metrics)
    except Exception as error:
        wb.macro('Turn_On_Features')()
        print('we exited unexpectedly and reneabled excel operation')
        print('[Error]: %s'%error)
    wb.macro('Turn_On_Features')()



def get_system_results(mod_encoder_name, ref_encoder_name,sheet):
    global SYSTEM_HEADER
    headers = sheet.range('AZ5:BR5').value
    system_results = sheet.range('AZ6:BR6').value
##    print('temp_BDR_RESULTS[12]',temp_BDR_RESULTS[12])
    if not SYSTEM_HEADER:
       SYSTEM_HEADER = [None]*len(system_results)
    for index,system_result in enumerate(system_results):
        if system_result != None:
            SYSTEM_HEADER[index] = headers[index]
            
    # temp=[x for x in temp_BDR_RESULTS[0:4] + [temp_BDR_RESULTS[7]] + [temp_BDR_RESULTS[11]]+ [temp_BDR_RESULTS[12]] if x !=None]
    system_result=[x for x in system_results if x!= None]
    print('system_result',system_result)

    SYSTEM_RESULTS.append(system_result)
##    encode_sum_cycles = master_sheet.range('BG19:BG21').value
##    decode_sum_cycles = master_sheet.range('BH19:BH21').value
##    
##    ref_sum_cycles.append([mod_encoder_name]+['vs']+[ref_encoder_name]+[encode_sum_cycles[0]]+[decode_sum_cycles[0]])
##    mod_sum_cycles.append([mod_encoder_name]+['vs']+[ref_encoder_name]+[encode_sum_cycles[1]]+[decode_sum_cycles[1]])
##
##    print('ref_sum_cycles',ref_sum_cycles)
##    print('mod_sum_cycles',mod_sum_cycles)

    
def get_bdr_results(sheet):
    global BDR_HEADER
    headers = sheet.range('AZ2:BR2').value
    bdr_results=sheet.range('AZ3:BR3').value
##    print('temp_BDR_RESULTS[12]',temp_BDR_RESULTS[12])
    if not BDR_HEADER:
       BDR_HEADER = [None]*len(bdr_results)
    for index,temp_cvh_result in enumerate(bdr_results):
        if temp_cvh_result != None and temp_cvh_result != 1.0:
            BDR_HEADER[index] = headers[index]
            
    # temp=[x for x in temp_BDR_RESULTS[0:4] + [temp_BDR_RESULTS[7]] + [temp_BDR_RESULTS[11]]+ [temp_BDR_RESULTS[12]] if x !=None]
    bdr_result=[x for x in bdr_results if (x !=1.0 and x!= None)]
    BDR_RESULTS.append(bdr_result)

    
def set_encoder_names(mod_encoder_name, ref_encoder_name, sheet):
    print('mod_encoder_name',mod_encoder_name)
    print('ref_encoder_names',ref_encoder_name)
    print('sheet',sheet)
    sheet.range('B28').value = ref_encoder_name
    sheet.range('AA28').value = mod_encoder_name


def adjust_excel_cvh(number_of_qps,number_of_cvh, stop_point, cvh_comparison, sheet):
    bdr_table = list()
    system_table = list()
    if cvh_comparison == '2':
        bdr_points = max(number_of_qps,number_of_cvh)
    else: 
        bdr_points = number_of_qps

    #=IFERROR(@bdRateExtend($H32:$H70,G32:G70,$AA32:$AA70,Z32:Z70,,,$C$1,$C$2),"N/A")
    bdr_form='''=IFERROR(@bdRateExtend($%s%s:$%s%s,%s%s:%s%s,$%s%s:$%s%s,%s%s:%s%s),"N/A")'''
    bdr_average_form = '''=IFERROR((BC{bdr_low}+BG{bdr_low}+BK{bdr_low})/3,"n/a")'''
    speed_dev_form = '''=IFERROR((SUM(Q{system_low}:Q{system_high})+SUM(R{system_low}:R{system_high}))/(SUM(AP{system_low}:AP{system_high})+SUM(AQ{system_low}:AQ{system_high}))-1,"N/A")'''
    memory_dev_form = '''=IFERROR((MAX(AO{system_low}:AO{system_high})/MAX(P{system_low}:P{system_high}))-1,"0")'''
    starting_row=32
    bdr_low=starting_row
    system_low = starting_row

    for point_num in range(stop_point):
        if point_num % (bdr_points)==0:
            bdr_high=point_num+starting_row+bdr_points-1
            bdr_table.extend([[bdr_form%('E',bdr_low,'E',bdr_high,'F',bdr_low,'F',bdr_high,'AD',bdr_low,'AD',bdr_high,'AE',bdr_low,'AE',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'G',bdr_low,'G',bdr_high,'AD',bdr_low,'AD',bdr_high,'AF',bdr_low,'AF',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'H',bdr_low,'H',bdr_high,'AD',bdr_low,'AD',bdr_high,'AG',bdr_low,'AG',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'I',bdr_low,'I',bdr_high,'AD',bdr_low,'AD',bdr_high,'AH',bdr_low,'AH',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'J',bdr_low,'J',bdr_high,'AD',bdr_low,'AD',bdr_high,'AI',bdr_low,'AI',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'K',bdr_low,'K',bdr_high,'AD',bdr_low,'AD',bdr_high,'AJ',bdr_low,'AJ',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'L',bdr_low,'L',bdr_high,'AD',bdr_low,'AD',bdr_high,'AK',bdr_low,'AK',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'M',bdr_low,'M',bdr_high,'AD',bdr_low,'AD',bdr_high,'AL',bdr_low,'AL',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'N',bdr_low,'N',bdr_high,'AD',bdr_low,'AD',bdr_high,'AM',bdr_low,'AM',bdr_high),\
                                  bdr_form%('E',bdr_low,'E',bdr_high,'O',bdr_low,'O',bdr_high,'AD',bdr_low,'AD',bdr_high,'AN',bdr_low,'AN',bdr_high),\
                                  bdr_average_form.format(**vars())
                                   ]])
            bdr_low=bdr_high+1
        else:
            bdr_table.extend([['','','','','','','','','','','']])
        if point_num % number_of_qps == 0:
            system_high=point_num+starting_row+number_of_qps-1

            system_table.extend(  [[speed_dev_form.format(**vars()),\
                                  memory_dev_form.format(**vars())]])
            system_low = system_high + 1
        else:
            system_table.extend([['','']])
##    print('bdr_table',bdr_table)
    sheet.range('BC32').value =  bdr_table
    sheet.range('BN32').value =  system_table
    
'''Code retrieved from https://stackoverflow.com/questions/2130016/splitting-a-list-into-n-parts-of-approximately-equal-length'''
def segment_list(list_target, segments):
    k, m = divmod(len(list_target), segments)
    return list((list_target[i*k+min(i, m):(i+1)*k+min(i+1, m)] for i in range(segments)))

def segment_metrics(results, number_of_segments,number_of_presets):
    
    segmented_list = segment_list(results, number_of_segments)
    vertically_merged_list = [x[3] for tup in zip(*segmented_list) for x in list(tup)]
    
    final_metric_list = segment_list(vertically_merged_list, number_of_presets)
    return final_metric_list

def set_results(summary_range, cvh_comparison, mod_metrics):
    global BDR_HEADER
    global SYSTEM_HEADER
    
    result_set = list()

    sorted_system_results = sorted([x for x in SYSTEM_RESULTS], key = lambda x:(x[0].split('_')[0], int(x[0].split('_')[-1][1:])))


    BDR_HEADER = [x for x in BDR_HEADER if x != None]
    SYSTEM_HEADER = [x for x in SYSTEM_HEADER if x != None]
##    print('\n')
##    print('BDR_HEADER',BDR_HEADER)
##    print('SYSTEM_HEADER',SYSTEM_HEADER)
##    print('\n')
    header = BDR_HEADER + SYSTEM_HEADER#['MOD Encode Cycles', 'REF Encode Cycles', 'MOD Decode Cycles', 'REF Decode Cycles']
##    print('sorted_bdr_results',)
    
    if cvh_comparison== '2':
        sorted_bdr_results=sorted([x for x in BDR_RESULTS], key = lambda x:(re.findall(r'.*?_M\d+_(.*)',x[0])[0], x[0].split('_')[0], int(re.findall(r'_M(\d+)',x[0])[0])))
        
        if mod_metrics:
            segmented_metrics = segment_metrics(sorted_bdr_results, len(mod_metrics),len(sorted_system_results))

        for metric_results, system_results in zip(segmented_metrics, sorted_system_results):
            average_bdr = sum(metric_results)/len(metric_results)
            result_set.append(system_results[:3] + metric_results +[average_bdr]+ system_results[3:])
    else:
        sorted_bdr_results=sorted([x for x in BDR_RESULTS], key = lambda x:(x[0].split('_')[0], int(x[0].split('_')[-1][1:])))

        for metric_results, system_results in zip(sorted_bdr_results, sorted_system_results):
            result_set.append(metric_results + system_results[3:])
        
    sorted_result_set=sorted([x for x in result_set], key = lambda x: (int(x[0].split('_')[-1][1:]), x[2].split('_')[0]))
    sorted_result_set.insert(0, header)
    
    print(sorted_result_set)
    
    summary_sheet.range('A%s'%str(summary_range)).value = sorted_result_set
    header_range = summary_sheet.range('A%s:%s%s'%(str(summary_range),chr(len(header)-1 + 97),str(summary_range)))
    
    header_range = header_range.expand('right')
    header_range.color = (112,173,71)
    header_range.api.Font.Color = 0xFFFFFF
    header_range.api.Font.Bold = True
    header_range.api.Font.Size = 11
    
    id_column_range = summary_sheet.range('A%s'%str(summary_range+1)).expand('down')
    id_column_range.color=(198,224,180)

    data_ex_headers_range = summary_sheet.range('A%s'%str(summary_range)).expand('table')
    for border_id in range(7,13):
        data_ex_headers_range.api.Borders(border_id).Weight = 2
        data_ex_headers_range.api.Borders(border_id).LineStyle = 1
        data_ex_headers_range.api.Borders(border_id).Color = 0x000000
    

def uniform_search(sheet):
    complete_column=sheet.range('B:B').value
    for i in range(len(complete_column)-1,-1,-1):
        if complete_column[i]!=None:
            return i+3
    return 1
        
def chunk_data(all_data):
    for i in range(0, len(all_data), 100000):
        yield all_data[i:i + 100000]
        

def perform_comparisons(mod_encoder_names, ref_encoder_names, mod_metrics,ref_metrics, cvh_comparison, comparison_selection):
    if comparison_selection == '1':
        for mod_encoder_name in mod_encoder_names:
            set_encoder_names(mod_encoder_name, ref_encoder_names[0], master_sheet)
            get_system_results(mod_encoder_name, ref_encoder_names,master_sheet)
            if cvh_comparison == '2':
                for mod_metric,ref_metric in zip(mod_metrics,ref_metrics):
                    set_encoder_names(mod_metric % mod_encoder_name, ref_metric % ref_encoder_names[0], master_cvh_sheet)
                    get_bdr_results(master_cvh_sheet)
            else:
                get_bdr_results(master_sheet)
    else:        
        for mod_encoder_name, ref_encoder_name in zip(mod_encoder_names, ref_encoder_names):
            set_encoder_names(mod_encoder_name, ref_encoder_name, master_sheet)
            get_system_results(mod_encoder_name, ref_encoder_name, master_sheet)
            if cvh_comparison == '2':
                for mod_metric,ref_metric in zip(mod_metrics,ref_metrics):
                    set_encoder_names(mod_metric % mod_encoder_name, ref_metric % ref_encoder_name, master_cvh_sheet)
                    get_bdr_results(master_cvh_sheet)
            else:
                get_bdr_results(master_sheet)      
    

def get_ref_mod_encoder_names(enc_selection, comparison_selection):
    mod_name = enc_selection[0].split('|')[1]
    presets = enc_selection[1]
    mod_encoder_names = ['{}_{}'.format(mod_name,preset) for preset in presets]
    ref_encoder_names = enc_selection[0].split('|')[0]

    if comparison_selection == '2':
        ref_encoder_names = ['{}_{}'.format(ref_encoder_names, preset) for preset in presets]
    else:
        ref_encoder_names = [ref_encoder_names]

    print('ref_encoder_names: ', ref_encoder_names)
    print('mod_encoder_names: ', mod_encoder_names)
    return mod_encoder_names, ref_encoder_names


def check_cvh_comparison_options(squashed_encoder_list, mod_encoder_names, ref_encoder_names):
    mod_cvh_options  = list()
    ref_cvh_options = list()
    ref_metrics = set()
    mod_metrics = set()
    
    for enc_name in squashed_encoder_list[1:]:
        if not enc_name[-1].isdigit():
            if re.search('(.*?_M\d+)',enc_name):
                enc_root = re.search('(.*?_M\d+)',enc_name).group(1)
            else:
                continue

            if enc_root in mod_encoder_names:
                mod_cvh_options.append(enc_name)
            if enc_root in ref_encoder_names:
                ref_cvh_options.append(enc_name)
    
    for mod_cvh_option in mod_cvh_options:
        metric_part = re.search(r'.*?_M\d+_(.*)',mod_cvh_option).group(1)
        mod_metrics.add('%s_{}'.format(metric_part))
    for ref_cvh_option in ref_cvh_options:
        metric_part =re.search(r'.*?_M\d+_(.*)',ref_cvh_option).group(1)
        ref_metrics.add('%s_{}'.format(metric_part))
    ref_metrics = sorted(list(ref_metrics))
    mod_metrics = sorted(list(mod_metrics))
    
    print('ref_metrics',ref_metrics)
    print('mod_metrics',mod_metrics)        
    if mod_metrics and ref_metrics and len(mod_metrics) == len(ref_metrics):
        print('The option to do CVH comparisons are available.\nSelect comparison Type.\n(1): Face to Face\n(2): Convex Hull')
        cvh_comparison = str(input('Select Option -> '))
    else:
        cvh_comparison = 0
        

    return cvh_comparison, mod_metrics,ref_metrics

def get_qp_cvh_points(encoder_names_from_all_data, sequence_names_from_all_data, encoder_name, qp_values_from_all_data):
    initial_sequence_name = None
    cvh_qps = list()
    for encoder_name_from_all_data, sequence_name, cvh_qp in zip(encoder_names_from_all_data, sequence_names_from_all_data, qp_values_from_all_data):
        if encoder_name_from_all_data == encoder_name:
            if initial_sequence_name and sequence_name != initial_sequence_name:
                break
            
            initial_sequence_name = sequence_name
            cvh_qps.append(cvh_qp)
    
    return cvh_qps



def get_number_of_points(mod_metrics,ref_metrics, encoder_names_from_all_data, sequence_names_from_all_data,mod_encoder_names,ref_encoder_names, qp_values_from_all_data, cvh_comparison):
    mod_qps = get_qp_cvh_points(encoder_names_from_all_data, sequence_names_from_all_data, mod_encoder_names[0], qp_values_from_all_data)
    ref_qps = get_qp_cvh_points(encoder_names_from_all_data, sequence_names_from_all_data, ref_encoder_names[0], qp_values_from_all_data)
    if mod_metrics and ref_metrics:
        mod_cvh = get_qp_cvh_points(encoder_names_from_all_data, sequence_names_from_all_data, mod_metrics[0] % mod_encoder_names[0], qp_values_from_all_data)
        ref_cvh = get_qp_cvh_points(encoder_names_from_all_data, sequence_names_from_all_data, ref_metrics[0] % ref_encoder_names[0], qp_values_from_all_data)
    
    if cvh_comparison == '2' and len(mod_cvh) == len(ref_cvh):
        number_of_cvh_points = len(mod_cvh)
    elif cvh_comparison == '2' and len(mod_cvh) != len(ref_cvh):
        print('mod_cvh', mod_cvh)
        print('ref_cvh', ref_cvh)
        print('[CRITICAL ERROR]: The cvh values are not matching.')
##        sys.exit()
        number_of_cvh_points = len(mod_cvh)

    else: 
        number_of_cvh_points = 0
    
    if mod_qps == ref_qps:
        number_of_qps = len(mod_qps)
    else:
        print('mod_qps', mod_qps)
        print('ref_qps', ref_qps)
        print('[CRITICAL ERROR]: The QPs are not matching.')
        number_of_qps = len(mod_qps)

##        sys.exit()

    print('number_of_cvh_points', number_of_cvh_points)
    print('number_of_qps', number_of_qps)
    return number_of_cvh_points, number_of_qps

    
def get_comparison_type_selection(squashed_encoder_list):
    print('\n(1) : n0,n1,n2... vs n0 \n(2) : n0,n1,n2... vs n0,n1,n2...')
    comparison_selection = None
    while comparison_selection != '1' and comparison_selection != '2':
        comparison_selection = str(input('Select the which comparison type to perform -> '))
        if comparison_selection.strip() not in ['1','2']: ## comparison_selection != '1' and comparison_selection != '2' or comparison_selection.strip().isdigit() == False
            print('Bad Choice, Try again...')
    for index, enc_name in enumerate(squashed_encoder_list):
        if enc_name[-1].isnumeric():
            enc_group = '_'.join(enc_name.split('_')[0:-1])
            enc_preset = enc_name.split('_')[-1]
            if enc_group not in grouped_encoders:
                grouped_encoders[enc_group] = [enc_preset]
            else:
                grouped_encoders[enc_group].append(enc_preset)
            if comparison_selection == '1':
                print('({0}) -> {1}'.format(index, enc_name))
            
    paired_encoders = dict()
    if comparison_selection == '1':
        ref_selection = int(input('Select the reference encoder to use -> '))
        ref_enc = squashed_encoder_list[ref_selection]
        for mod_enc in grouped_encoders:
            if mod_enc != ref_enc:
                mod_presets = grouped_encoders[mod_enc]
                pair_id = '{0}|{1}'.format(ref_enc,mod_enc)
                paired_encoders[pair_id] = sorted(mod_presets, key = lambda x: int(x.replace('M','')))
    else:
        for ref_enc in grouped_encoders:
            for mod_enc in grouped_encoders:
                if mod_enc != ref_enc:
                    ref_presets = grouped_encoders[ref_enc]
                    mod_presets = grouped_encoders[mod_enc]

                    common_presets = [preset for preset in ref_presets if preset in mod_presets]
                    if common_presets:
                        pair_id = '|'.join('{0}|{1}'.format(ref_enc,mod_enc).split('|'))
                        print('pair_id',pair_id)
                        paired_encoders[pair_id] = common_presets
                        pair_id = '|'.join('{1}|{0}'.format(ref_enc,mod_enc).split('|'))
                        paired_encoders[pair_id] = common_presets
                        
    for index, pair in enumerate(paired_encoders):
        print('\n({0}) : {1} | {2}'.format(index,pair,paired_encoders[pair]))

    
    while True:
        pairs = list(paired_encoders.items())
        pair_selection = str(input('Select the pair to compare -> '))
        if pair_selection.strip().isdigit() == False or int(pair_selection) >= len(pairs) or int(pair_selection) < 0:
            print('Invalid Selection, Retry?')
            continue
        else:
            enc_selection = pairs[int(pair_selection)]
            print(enc_selection)
            break
            
    return comparison_selection, enc_selection


def set_data(result_files, data_range):
    encoder_names_from_all_data = all_data_sheet.range("B:B").value

    squashed_encoder_list = sorted([x for x in set(encoder_names_from_all_data) if x != None])

    all_data=[]
        
    for result_file in result_files:
        mod_encoder_names = get_encoder_names([result_file], 1)
        
        if all([x not in squashed_encoder_list for x in mod_encoder_names]):
            if 'convex_hull' in result_file:
                for qp_overwrite, line in enumerate(open(result_file)):
                    line_split = line.split('\t')
                    if len(line_split) > 5 and line_split[5].strip().isnumeric():
                        line_split[5] = str(qp_overwrite)
                        line_split[-1] = line_split[-1].replace('\n','')
                        
                        all_data.append(line_split)
            else:
                for line in open(result_file):
                    line_split = line.split('\t')
                    if len(line_split) > 5 and line_split[5].strip().isnumeric():
                        line_split[-1] = line_split[-1].replace('\n','')
                        all_data.append(line_split)
        else:
            print('{} found in all data page, skipping data insertion...\n'.format(mod_encoder_names))
            
    for index, data in enumerate(chunk_data(all_data)):
##        for sub_data in data:
##            columns = len(sub_data)
####            print('columns',columns)
####            if columns != 29:
##            print('columns',len(sub_data))
##            print('sub_data',sub_data)
        cell = 'A{}'.format(data_range + (index * 100000) + index)
        all_data_sheet.range(cell).value = data
        
    wb.macro('Refresh_Data')()


def verify_result_file_integrity(result_files):
    '''Adding the total number of cvh points to a set, there should be one single value in the set given that all cvh results have same number of points'''
    cvh_points = set()
    qps = set()
    valid_result = True
    reason = None
    for result_file in result_files:
        empty_lines = 0
        if 'convex_hull' in result_file:
            seq_names = set()
            for index,x in enumerate(open(result_file)):
                line_split = x.split('\t')

                if len(line_split) > 5 and line_split[5].strip().isnumeric():
                    seq_name=line_split[4]
                    seq_names.add(seq_name)
                    
                    if len(seq_names) > 1:
                        number_of_cvh_points = index - empty_lines
##                        print('number_of_cvh_points',number_of_cvh_points,'file',result_file)
                        cvh_points.add(number_of_cvh_points)
                        break
                else:
                    empty_lines += 1
        else:
            temp_qps = set()
            for index, x in enumerate(open(result_file)):
                line_split = x.split('\t')
                qp = line_split[5].strip()
                
                if len(line_split) > 5 and qp.isnumeric():
                    if qp in temp_qps:
                        if not qps:
                            qps = temp_qps
                        elif temp_qps != qps:
                        
                            valid_result = False
                            reason = 'The number of QPs are not uniform, missing QP? {}'.format(str(qps))
                        temp_qps = set()
                    else:
                        temp_qps.add(qp)

    if len(cvh_points) > 1:
        valid_result = False
        reason = 'The number of convex hull points are not uniform, {}'.format(str(cvh_points))
    return valid_result, reason


def get_encoder_names(result_files, convex=0):
    encoder_names=[]
    for result_file in result_files:
        if ('convex_hull' not in result_file and not convex) or (convex):
            with open(result_file,'r') as result:
                for index,encoder_name in enumerate(result.readlines()):
                    if index>0 and encoder_name != "\n":
                        encoder_names.append(encoder_name.split('\t')[1])
    return sorted(list(set(encoder_names)))



if __name__ == '__main__':   
    t=time.time()
    main()
    print(time.time() - t)
