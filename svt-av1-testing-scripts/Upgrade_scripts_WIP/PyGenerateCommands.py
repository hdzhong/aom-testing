import os, sys, re, glob, stat, subprocess, binascii, multiprocessing 
from datetime import date
from itertools import product

unit_test = ['SPIE2021_svt', 'SPIE2021_aom', 'SPIE2021_x264', 'SPIE2021_x265', 'SPIE2021_vp9']

'''Add special parameters to commands for testing (i.e. --pred-struct 1, --tune 0)'''
INSERT_SPECIAL_PARAMETERS = []

TEST_SETTINGS = {
    'test_config' : 'SPIE2021_svt',
    'stream_dir' : r'/home/ubuntu/stream/ElFuente',
    'presets' : [13],
    'intraperiod' : -1,
    'num_pool' : 96,
    'ffmpeg_job_count' : 20,
    'vmaf_job_count' : 96,
}

check_script_for_bugs = 0

def test_configuration():
    TEST_COMMAND_REPOSITORY = {
        'SPIE2021_svt'            :     [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['SPIE2021_ffmpeg_vmaf_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p'],],                    
        'SPIE2021_aom'            :     [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['SPIE2021_ffmpeg_vmaf_rescale'], ENCODE_COMMAND['aom_CRF_2p'],],                    
        'SPIE2021_x264'            :     [RC_VALUES['SPIE2021_x264_x265'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['SPIE2021_ffmpeg_vmaf_rescale'], ENCODE_COMMAND['x264_CRF_1p'],],                    
        'SPIE2021_x265'            :     [RC_VALUES['SPIE2021_x264_x265'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['SPIE2021_ffmpeg_vmaf_rescale'], ENCODE_COMMAND['x265_CRF_1p'],],                    
        'SPIE2021_vp9'            :     [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['SPIE2021_ffmpeg_vmaf_rescale'], ENCODE_COMMAND['vp9_CRF_2p_SPIE2021'],],                    


        'ELFUENTE_ffmpeg_svt_fast_decode' :  [RC_VALUES['SPIE2021_svt_aom'], RESOLUTION['SPIE2021_8bit'], DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['SPIE2021_ffmpeg_vmaf_rescale'], ENCODE_COMMAND['ffmpeg_svt_fast_decode'],],
        'NETFLIX_svt' :                      [RC_VALUES['netflix_crf'],  RESOLUTION['netflix'],  DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['netflix_ffmpeg_vmaf_exe_rescale'], ENCODE_COMMAND['svt_CRF_1lp_1p'],],
        # 'NETFLIX_svt_pseudo_run' :           [[63],  RESOLUTION['netflix'],  DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['ffmpeg_vmaf'],         ENCODE_COMMAND['svt_CRF_1lp_1p'],],
        
        'libaom_2p' :                      [RC_VALUES['netflix_crf'],  RESOLUTION['netflix'],  DOWNSCALE_COMMAND['ffmpeg_lanczos'], METRIC_COMMAND['netflix_ffmpeg_vmaf_exe_rescale'], ENCODE_COMMAND['aom_CRF_2p'],],

        }
    return TEST_COMMAND_REPOSITORY

import shutil
def verify_script_functionality():
    TEST_COMMAND_REPOSITORY = test_configuration()
    for feature_to_test in unit_test:
        test_feature(feature_to_test,TEST_COMMAND_REPOSITORY)
    
        test_dir = os.path.join(os.getcwd(), 'test')
        config_dir = os.path.join(test_dir, feature_to_test)
        if not os.path.isdir(test_dir):
            os.mkdir(test_dir)
        if not os.path.isdir(config_dir):
            os.mkdir(config_dir)
        # print('config_params',config_params)
        cwd_files = glob.glob('%s/*'%os.getcwd())
        # print('done')
           
        for cwd_file in cwd_files:
            file_name = os.path.split(cwd_file)[-1]
            # print('file_name',file_name)
            if file_name == 'test':
                continue
            if file_name not in ['PyGenerateCommandsV22_6_15.py','tools','test','encoders']:
                shutil.move(cwd_file, os.path.join(config_dir,file_name))
            else:
                try:
                    shutil.copytree(cwd_file, os.path.join(config_dir,file_name))
                except Exception as e: # python >2.5
                    print(e)
                    print('error filename',file_name)
                    shutil.copy(cwd_file, os.path.join(config_dir,file_name))
                # shutil.copy(cwd_file, os.path.join(config_dir,file_name))
                

            # break
        
##    config_type = [y for x in all_config_types for y in x]
##    config = 







RESOLUTION = {
    'netflix' : [(2560,1440),(1920,1080),(1280,720),(960,540),(768,432),(608,342),(480,270),(384,216),(2560,1088),(1920,816),(1280,544),(960,408),(748,318),(588,250),(480,204),(372,158)],

    'SPIE2020_10bit' : [(1280,720),(960,540), (768,432), (608,342), (480,270), (384,216)],   
    'SPIE2020_8bit' : [(1280,720),(960,540),(640,360),(480,270)],
    'SPIE2021_8bit' : [(1280,720),(960,540),(768,432),(640,360),(512,288),(384,216),(256,144)],
    }


RC_VALUES = {
    'netflix_crf'               : [16,20,24,28,32,36,39,43,47,51,55,59,63],
    
    'SPIE2021_x264_x265'    : [19,21,23,25,27,29,31,33,35,37,41], 
    'SPIE2021_svt_aom'      : [23,27,31,35,39,43,47,51,55,59,63],
    
    'generic_tbr'           : [5000,4000,3000,2000]

    }


DOWNSCALE_COMMAND = {
    'ffmpeg_lanczos' : "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags lanczos+accurate_rnd+full_chroma_int -sws_dither none -param0 5  -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {new_clip_name}",
    'ffmpeg_bilinear': "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags bilinear+accurate_rnd+full_chroma_int -sws_dither none -param0 5 -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {new_clip_name}",
    'ffmpeg_bicubic' : "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags bicubic+accurate_rnd+full_chroma_int -sws_dither none -param0 5  -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {new_clip_name}",
    'ffmpeg_gauss'   : "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags gauss+accurate_rnd+full_chroma_int -sws_dither none -param0 5    -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {new_clip_name}",
    'ffmpeg_sinc'    : "tools/ffmpeg  -y -f {rawvideo} -s:v {width}x{height} -pix_fmt {pixfmt} -r {fps} -i {ref_clip} -sws_flags sinc+accurate_rnd+full_chroma_int -sws_dither none -param0 5     -strict -1 -f {rawvideo} -s:v {target_width}x{target_height} -pix_fmt {pixfmt} -r {fps} {new_clip_name}",

    'HDRConvert'    : 'tools/HDRConvert -p SourceFile="{ref_clip}" -p OutputFile="{new_clip_name}" -p SourceWidth={width} -p SourceHeight={height} -p OutputWidth={target_width} -p OutputHeight={target_height} -p SourcebitdepthCmp0={bitdepth} -p SourcebitdepthCmp1={bitdepth} -p SourcebitdepthCmp2={bitdepth} -p OutputbitdepthCmp0={bitdepth} -p OutputbitdepthCmp1={bitdepth} -p OutputbitdepthCmp2={bitdepth} -p SourceRate={fps} -p NumberOfFrames={number_of_frames} -p OutputRate={fps} -p SourceChromaFormat=1 -p SourceFourCCCode=0 -p SourceColorSpace=0 -p SourceColorPrimaries=1 -p OutputChromaFormat=1 -p OutputColorSpace=0 -p OutputColorPrimaries=1 -p SilentMode=1 -p ScaleOnly=1'

}


METRIC_COMMAND = {


    


    'vvenc_ffmpeg_vmaf_exe'  : '''(./vvdecapp -b {output_filename}.bin -o {temp_clip} -t 1 && tools/vmaf --reference {clip} --distorted {temp_clip} -w {width} -h {height} -p 420{vmaf_pixfmt} --aom_ctc v1.0 -b {bitdepth} -o {output_filename}.xml && rm {temp_clip}) > {output_filename}.log 2>&1 ''',
    
    'SPIE2020_ffmpeg_psnr_ssim' : '''(/usr/bin/time --verbose tools/ffmpeg -nostdin -r 25 -i {output_filename}.bin -s {width}x{height} -f {rawvideo} -pix_fmt {pixfmt} -r 25 -i {clip} -lavfi "ssim=stats_file={output_filename}.ssim;[0:v][1:v]psnr=stats_file={output_filename}.psnr" -f null - ) > {output_filename}.log 2>&1''',
    'SPIE2020_ffmpeg_vmaf'        : r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -s {width}x{height} -f {rawvideo} -pix_fmt {pixfmt} -r 25 -i {clip} -lavfi '[0:v][1:v]libvmaf=model=version=vmaf_v0.6.1\\:name=vmaf|version=vmaf_v0.6.1neg\\:name=vmaf_neg:feature=name=psnr\\:reduced_hbd_peak=true\\:enable_apsnr=true\\:min_sse=0.5|name=float_ssim\\:enable_db=true\\:clip_db=true:log_path={output_filename}.xml:log_fmt=xml' -threads 1 -f null - ) > {output_filename}.log 2>&1''',
    'SPIE2020_ffmpeg_psnr_ssim_vmaf_rescale'    : '''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -r 25 -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+print_info:sws_dither=none:param0=5:threads=1 [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; [scaled1][1:v]ssim=stats_file={output_filename}.ssim;[scaled2][1:v]psnr=stats_file={output_filename}.psnr;[scaled3][1:v]libvmaf=model_path=tools/model/vmaf_v0.6.1.pkl:log_path={output_filename}.vmaf' -map "[ref]" -f null - ) > {output_filename}.log 2>&1''',
    'SPIE2020_ffmpeg_psnr_ssim_vmaf'            : '''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -r 25 -pix_fmt {pixfmt} -s:v {mod_width}x{mod_height} -i {clip} -lavfi 'ssim=stats_file={output_filename}.ssim;[0:v][1:v]psnr=stats_file={output_filename}.psnr;[0:v][1:v]libvmaf=model_path=tools/model/vmaf_v0.6.1.pkl:log_path={output_filename}.vmaf' -f null - ) > {output_filename}.log 2>&1''',

    'SPIE2021_ffmpeg_vmaf'            : r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -r 25 -pix_fmt {pixfmt} -s:v {mod_width}x{mod_height} -i {clip} -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -f null - > {output_filename}.log 2>&1''',
    'SPIE2021_ffmpeg_vmaf_rescale'    : r'''(/usr/bin/time --verbose tools/ffmpeg -y -nostdin  -r 25 -i {output_filename}.bin -f {rawvideo} -r 25 -pix_fmt {pixfmt} -s:v {ref_width}x{ref_height} -i {ref_clip} -lavfi 'scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:threads=1 [scaled][ref];[scaled] split=1 [scaled1]; [scaled1][1:v]libvmaf=aom_ctc=1:log_path={output_filename}.xml:log_fmt=xml' -map "[ref]" -f null - ) > {output_filename}.log 2>&1''',

    'netflix_ffmpeg_vmaf_exe_rescale' : '''(tools/ffmpeg -y -r 25 -i {output_filename}.bin -s:v {ref_width}x{ref_height}  -pix_fmt {pixfmt} -f {rawvideo} -r 25 -i {clip}  -lavfi "scale2ref=flags=lanczos+accurate_rnd+print_info:threads=1 [scaled][ref];[scaled] split=2 [scaled1][scaled2]; [scaled1][1:v]psnr=stats_file={output_filename}.psnr" -map "[ref]" -f null - -map "[scaled2]" -strict -1 -pix_fmt {pixfmt} -f {rawvideo} {temp_clip} && tools/vmaf --reference {clip} --distorted {temp_clip} --width {ref_width} --height {ref_height} --pixel_format {vmaf_pixfmt} --bitdepth {bitdepth} --output {output_filename}.xml --model path=./tools/model/vmaf_4k_v0.6.1.json:name=vmaf --feature psnr --threads 19 && tools/vmaf --reference {clip} --distorted {temp_clip} --width {ref_width} --height {ref_height} --pixel_format {vmaf_pixfmt} --bitdepth {bitdepth} --output {output_filename}.xml_neg --model path=./tools/model/vmaf_4k_v0.6.1.json:vif.vif_enhn_gain_limit=1.0:adm.adm_enhn_gain_limit=1.0 --feature psnr --threads 19  && rm {temp_clip}) > {output_filename}.log 2>&1''',
    'SPIE2021_ffmpeg_vmaf_exe_rescale' : '''(/usr/bin/time --verbose tools/ffmpeg -y -r 25 -i {output_filename}.bin -s:v {ref_width}x{ref_height} -pix_fmt {pixfmt} -f {rawvideo} -r 25 -i {ref_clip}  -lavfi "scale2ref=flags=lanczos+accurate_rnd+full_chroma_int:sws_dither=none:param0=5:threads=1 [scaled][ref]" -map "[ref]" -f null - -map "[scaled]" -strict -1 -pix_fmt {pixfmt} {temp_clip} && tools/vmaf --reference {ref_clip} --distorted {temp_clip} --width {ref_width} --height {ref_height} --pixel_format {vmaf_pixfmt} --bitdepth {bitdepth} --output {output_filename}.xml --aom_ctc v1.0 && rm {temp_clip})> {output_filename}.log 2>&1''',

}


ENCODE_COMMAND = {
    
    'svt_CRF_1lp_1p'  : '''(/usr/bin/time --verbose   ./SvtAv1EncApp -q {rc_value}   --preset {preset} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --lp 1  --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

    'svt_CRF_nonlp_1p': '''(/usr/bin/time --verbose   ./SvtAv1EncApp -q {rc_value}   --preset {preset} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom} --passes 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

    'svt_VBR_1lp_1p'  : '''(/usr/bin/time --verbose   ./SvtAv1EncApp --tbr {rc_value} --preset {preset} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  --lp 1  --passes 1 --rc 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',

    'svt_VBR_1lp_2p'  : '''(/usr/bin/time --verbose   ./SvtAv1EncApp --tbr {rc_value} --preset {preset} --keyint {intraperiod} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  --lp 1  --passes 2 --rc 1 --irefresh-type 2  -i  {clip} --stats {output_filename}.stat -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',

    'svt_CRF_1lp_2p'  : '''(/usr/bin/time --verbose   ./SvtAv1EncApp --preset {preset} -w {width} -h {height} --input-depth {bitdepth} --fps-num {fps_num} --fps-denom {fps_denom}  -q {rc_value}  --keyint  {intraperiod}  --lp 1 -i  {clip}  -b  {output_filename}.bin )  > {output_filename}.txt 2>&1''',

    'aom_CRF_2p'      : '''(/usr/bin/time --verbose   ./aomenc  --passes=2 --verbose  --lag-in-frames=35 --auto-alt-ref=1 --end-usage=q  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --width={width} --height={height} --fps={fps_num}/{fps_denom} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod}\
--cq-level={rc_value} --cpu-used={preset}  -o  {output_filename}.bin  {clip}  )  > {output_filename}.txt 2>&1 ''',

    'x265_CRF_1p'     : '''(/usr/bin/time --verbose   ./x265  --preset {preset}  --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth}  --tune  psnr  --stats {output_filename}.stat --crf {rc_value} --keyint {intraperiod}  --min-keyint {intraperiod} --pools 1  --no-scenecut   --no-wpp   {clip}  -o {output_filename}.bin)  > {output_filename}.txt 2>&1 ''',

    'x264_CRF_1p'     : '''(/usr/bin/time --verbose   ./x264  --preset {preset}  --input-res {width}x{height}  --fps {fps_num}/{fps_denom}  --input-depth {bitdepth}  --threads 1  --tune psnr  --stats {output_filename}.stat  --crf {rc_value}  --keyint {intraperiod}  --min-keyint {intraperiod}  --no-scenecut   -o {output_filename}.bin  {clip})  > {output_filename}.txt 2>&1 ''',

    'vvenc_CRF_1p'    : '''(/usr/bin/time --verbose   ./vvencapp  --input {clip}  --size {width}x{height}  --qp {rc_value}  --preset {vvenc_preset}  --format  {vvenc_pixfmt}  --internal-bitdepth {bitdepth} --intraperiod {intraperiod} --framerate {fps}  --threads 1  --output {output_filename}.bin )  > {output_filename}.txt 2>&1 ''',


    'vp9_CRF_2p_SPIE2020'  : '''(/usr/bin/time --verbose   ./vpxenc --verbose   --passes=2 --end-usage=q  --lag-in-frames=25 --auto-alt-ref=6  --threads=1  --profile=0  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --width={width} --height={height} --fps={fps_num}/{fps_denom} \
--kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --cq-level={rc_value} --cpu-used={preset}  -o  {output_filename}.bin  {clip}  )  > {output_filename}.txt 2>&1 ''',

    'vp9_CRF_2p_SPIE2021'      : '''(/usr/bin/time --verbose   ./vpxenc --ivf --codec=vp9  --tile-columns=0 --arnr-maxframes=7 --arnr-strength=5 --aq-mode=0 --bias-pct=100 \
--minsection-pct=1 --maxsection-pct=10000 --i420 --min-q=0 --frame-parallel=0 --min-gf-interval=4 --max-gf-interval=16 --verbose   --passes=2 --end-usage=q  --lag-in-frames=25 \
--auto-alt-ref=6  --threads=1  --profile=0  --bit-depth={bitdepth} --input-bit-depth={bitdepth} --fps={fps_num}/{fps_denom} --kf-min-dist={intraperiod} --kf-max-dist={intraperiod} --cq-level={rc_value} --cpu-used={preset} -o  {output_filename}.bin    {clip})  > {output_filename}.txt 2>&1''',

    'ffmpeg_svt_fast_decode' : '''(/usr/bin/time --verbose ./ffmpeg -y  -s:v {width}x{height}  -pix_fmt {pixfmt}  -r {fps} -f {rawvideo}  -i {clip}  -crf {rc_value}  -preset {preset}  -g {intraperiod}  -threads 1  -c:v libsvtav1  -f ivf   -svtav1-params lp=1:fast-decode=1  {output_filename}.bin ) > {output_filename}.txt 2>&1'''

}


YUV_PARAMS = ['width','height','ref_width','mod_width','ref_height','mod_height','bitdepth','fps_num','fps_denom','fps','vvenc_pixfmt','rawvideo','pixfmt','vmaf_pixfmt','frames']
MEMORY_CALIBRATION = []




def main():
    # if check_script_for_bugs:
        # verify_script_functionality()
    # else:
    TEST_COMMAND_REPOSITORY = test_configuration()

    if TEST_SETTINGS['test_config']:
        feature_to_test = TEST_SETTINGS['test_config']
    else:
        print('No test Config Specified. Exiting...')
        sys.exit()
    # for feature_to_test in unit_test:
    test_feature(feature_to_test,TEST_COMMAND_REPOSITORY)


def test_feature(feature, TEST_COMMAND_REPOSITORY):
    '''Local bitstreams folder'''
    source_dir = os.path.join(os.getcwd(),'bitstreams')
    encode_optimal_job_count = TEST_SETTINGS['num_pool']
    ffmpeg_optimal_job_count = TEST_SETTINGS['ffmpeg_job_count']
    metric_optimal_job_count = TEST_SETTINGS['vmaf_job_count']
    '''Pull test info from features repo'''
    test_name = feature #'%s_test' % feature.strip('-')
    rc_values = TEST_COMMAND_REPOSITORY[feature][0]
    resolutions = TEST_COMMAND_REPOSITORY[feature][1]
    downscale_template = TEST_COMMAND_REPOSITORY[feature][2]
    metric_command_template = TEST_COMMAND_REPOSITORY[feature][3]
    encoding_command_template = TEST_COMMAND_REPOSITORY[feature][4]
    presets = TEST_SETTINGS['presets']
    print('presets',presets)
##    print('encoding_command_template',encoding_command_template)
    metric_id = list(METRIC_COMMAND.keys())[list(METRIC_COMMAND.values()).index(metric_command_template)]
    
    if INSERT_SPECIAL_PARAMETERS:
        encoding_command_template = insert_new_parameters(encoding_command_template)
    
##    '''Run calibration run to determine the optimal number of jobs for encoding and decoding'''
##    encoding_calibration_commands, metric_calibration_commands, _,_ = process_command_template(encoding_command_template,metric_command_template,downscale_template, [63], [], metric_id,[12])
##    encode_optimal_job_count, metric_optimal_job_count = calibration_run(encoding_calibration_commands, metric_calibration_commands,resolutions,rc_values, presets)
    
    '''Generate commands for test being performed'''
    encoding_commands, metric_commands, copy_commands, downscale_commands = process_command_template(encoding_command_template,metric_command_template,downscale_template, rc_values, resolutions, metric_id,presets)
    
    encode_file_id, metric_file_id = write_commands_to_files(encoding_commands, metric_commands, copy_commands, downscale_commands, test_name, metric_id)

    generate_bash_driver_file(encoding_command_template, encode_file_id, metric_file_id, metric_id, resolutions, encode_optimal_job_count, metric_optimal_job_count,ffmpeg_optimal_job_count)


def calibration_run(encoding_calibration_commands, metric_calibration_commands, resolutions, rc_values, presets):
    with open('/proc/meminfo') as f:
        meminfo = f.read()
    matched = re.search(r'^MemTotal:\s+(\d+)', meminfo)
    if matched: 
        total_machine_memory = int(matched.group(1))/(1024**2)
        
    total_machine_memory = 185
    
    encoding_calibration_commands = encoding_calibration_commands[0]
    metric_calibration_commands = metric_calibration_commands[0]

    total_encode_mem_use = 0
    total_metric_mem_use = 0
    count = 0
    max_clip_size = 0
    
    '''get max size of clips in stream folder'''
    for clip in glob.glob('{}/*'.format(TEST_SETTINGS['stream_dir'])):
        clip_size = os.stat(clip).st_size/1000**3
        max_clip_size = max(clip_size, max_clip_size)
        
    print('Starting Encode Calibration Run...')

    encode_mem_results = execute_parallel_commands(20,[x.replace('bitstreams','/dev/shm/bitstreams').split('>')[0] for x in encoding_calibration_commands],os.getcwd())
    print('encode_mem_results',encode_mem_results)
    
    for mem_result in encode_mem_results:
        total_encode_mem_use += float(mem_result)*3
    total_encode_mem_use_gb = total_encode_mem_use/1000000
    average_encode_mem_per_clip = total_encode_mem_use_gb / len(encode_mem_results)
    encode_optimal_job_count = int((0.9 * total_machine_memory) / average_encode_mem_per_clip)

    print('Starting Metric Calibration Run...')
    '''Calcualte max metric memory use'''
    if 'tools/vmaf' in metric_calibration_commands[0]:
        metric_optimal_job_count = int((total_machine_memory/2) / max_clip_size)        
    else:
        metric_mem_results = execute_parallel_commands(20,[x.replace('/dev/shm/bitstreams','bitstreams').replace('bitstreams','/dev/shm/bitstreams').split('>')[0] for x in metric_calibration_commands],os.getcwd())
        for metric_mem_result in metric_mem_results:
            total_metric_mem_use += float(metric_mem_result)*3
        total_metric_mem_use_gb = total_metric_mem_use/1000000
        average_metric_mem_per_clip = total_metric_mem_use_gb / len(metric_mem_results)
        metric_optimal_job_count = int((0.9 * total_machine_memory) / average_metric_mem_per_clip)

    '''Calcualte max encoding memory use'''
    number_of_cores = multiprocessing.cpu_count()
    encode_optimal_job_count = min(encode_optimal_job_count,number_of_cores)
    metric_optimal_job_count = min(metric_optimal_job_count,number_of_cores)
    
                    # for i in range(10):#range(len(resolutions)+1):
                        # print('i',i)
                        # total_mem_use += float(mem_result) * ((1/0.6)*i)
                        # count += 1
                        # if count == 96:
                            # break
    print('encode_optimal_job_count',encode_optimal_job_count)    
    print('metric_optimal_job_count',metric_optimal_job_count)
                
    return encode_optimal_job_count, metric_optimal_job_count


def write_commands_to_files(encoding_commands, metric_commands, copy_commands, downscale_commands, test_name,metric_id):
    encode_file_id = 'run-{}'.format(test_name)
    metric_file_id = 'run-{}'.format(metric_id)
    for preset, encode_commands, metric_commands in zip(TEST_SETTINGS['presets'], encoding_commands, metric_commands):
        with open('{}-m{}.txt'.format(encode_file_id, preset), 'w') as encode_file,\
             open('{}-m{}.txt'.format(metric_file_id, preset), 'w') as metric_file:
           for command in encode_commands:
                encode_file.write('{}\n'.format(command))
           for command in metric_commands:
               metric_file.write('{}\n'.format(command))
           
    with open('run_copy_reference.txt','w') as copy_file,\
         open('run_downscale.txt','w') as downscale_file:
       for command in copy_commands:
            copy_file.write('{}\n'.format(command))
       for command in downscale_commands:
            downscale_file.write('{}\n'.format(command))
            
    return encode_file_id, metric_file_id
    

def insert_new_parameters(token):
    sub_string_first_half = token.rpartition('-i')[0] 
    sub_string_second_half = token.rpartition('-i')[2]

    for param in INSERT_SPECIAL_PARAMETERS:
        sub_string_first_half += param + " "

    sample_command = sub_string_first_half + "-i" + sub_string_second_half
    return sample_command


'''Core Sample Command processing functions'''
def process_command_template(encoding_command_template, metric_command_template, downscale_template, rc_values, resolutions, metric_id, presets):
    processed_encode_commands = list()
    processed_metric_commands = list()
    copy_commands,downscale_commands = ('','')
    print('\narrived')
    for preset in presets:
        if 'aomenc' in encoding_command_template and preset > 6:
            preset = 6
        
        per_preset_encode_commands = list()
        per_preset_metric_commands= list()
        clip_lists = sort_clip_list_by_complexity(TEST_SETTINGS['stream_dir'])
        
        if resolutions:
            copy_commands, downscale_commands, clip_lists, parameter_tracker = get_downscaled_clip_list(clip_lists,resolutions, downscale_template)

        for i in range(len(clip_lists)):
            for rc_value in rc_values:
                for clip in clip_lists[i]:
##                    print('clip',clip)
                    if clip.endswith('yuv') and resolutions:
                        width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = parameter_tracker[clip]
                    elif clip.endswith('y4m') and resolutions:
                        width, height, framerate,number_of_frames = parameter_tracker[clip]
                    elif clip.endswith('yuv') and yuv_library_found:
                        width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = get_YUV_PARAMS(clip)
                    elif clip.endswith('y4m') and ('SvtAv1EncApp' not in encoding_command_template and TEST_SETTINGS['intraperiod'] == -1):
                        width, height, framerate,number_of_frames = read_y4m_header(clip)   
                    elif (not clip.endswith('yuv') and not clip.endswith('y4m')) or (clip.endswith('yuv') and not yuv_library_found):
                        continue
##                    print('clip',clip)
##                    print('resolutions',resolutions)
                    '''Set the intraperiod to +1 number of frames for encoders that do not support -1 keyint'''
                    if 'SvtAv1EncApp' not in encoding_command_template and TEST_SETTINGS['intraperiod'] == -1:
                        intraperiod = number_of_frames + 1
                    else:
                        intraperiod = TEST_SETTINGS['intraperiod']
                        
                    '''Set the pixfmt according to the bitdepth'''
                    if clip.endswith('yuv'):
                        if bitdepth == 10:
                            pixfmt = "yuv420p10le"
                            vvenc_pixfmt = 'yuv420_10'  
                        else:
                            pixfmt = "yuv420p"
                            vvenc_pixfmt = 'yuv420'   
                            
                    '''Assign the rc identifier based on the number of digits'''
                    if len(str(rc_value)) > 2:
                        rc_string = 'TBR{}kbps'.format(rc_value)
                    else:
                        rc_string = 'Q{}'.format(rc_value)
                        
                    '''Set the vvenc presets according to the preset number'''
                    vvenc_presets = ["slow", "medium", "fast", "faster"]
                    if preset < 4:
                        vvenc_preset = vvenc_presets[preset]
                    else:
                        vvenc_preset = "faster"

                    ref_width = mod_width = width
                    ref_height = mod_height = height
                    ref_clip = clip    

                    '''Get reference clip resolution for the rescale case'''
                    if resolutions:
                        ref_res = re.search(r'(\d+x\d+)to(\d+x\d+)',clip)
                        if not ref_res:
                            ref_width = mod_width = width
                            ref_height = mod_height = height
                            ref_clip = clip
                        else:
                            ref_width = ref_res.group(1).split('x')[0]
                            ref_height = ref_res.group(1).split('x')[1]
                            mod_width = ref_res.group(2).split('x')[0]
                            mod_height = ref_res.group(2).split('x')[1]
                            ref_clip = re.sub('to\d+x\d+_.*?_','_',clip)
                        
                    '''Generic setup of parameters and conditionals'''
                    clip_name = os.path.split(clip)[1]            
                    rawvideo = 'rawvideo'
                    vmaf_pixfmt = '420'
                    mod_encoding_command_template = encoding_command_template
                    mod_metric_command_template = metric_command_template
                    output_filename = '{}/{}_M{}_{}_{}'.format('bitstreams', TEST_SETTINGS['test_config'], preset, clip_name[:-4], rc_string)
                    
                    '''remove yuv tokens in case of y4m clip'''
                    if clip.endswith('y4m'):
                        encode_param_tokens = re.findall('\s*\S*\s*{.*?}',encoding_command_template)
                        metric_param_tokens = re.findall('\s*\S*\s*{.*?}',metric_command_template)
                        
                        for encode_param_token in encode_param_tokens:
                            encode_param = encode_param_token.split('{')[-1].split('}')[0]
                            if encode_param in YUV_PARAMS:
                                mod_encoding_command_template = mod_encoding_command_template.replace(encode_param_token,'')
                        for metric_param_token in metric_param_tokens:
                            metric_param = metric_param_token.split('{')[-1].split('}')[0]
                            if metric_param in YUV_PARAMS:
                                mod_metric_command_template = mod_metric_command_template.replace(metric_param_token,'') 
                                
                    '''sub in the values'''
                    if clip.endswith('y4m'):
                        temp_clip = '/dev/shm/{}.y4m'.format(output_filename)
                        '''Generate Encode Commands'''
                        processed_encoding_command = mod_encoding_command_template.format(**vars())
                        '''Generate Metric Command'''
                        processed_metric_command = mod_metric_command_template.format(**vars())
                    elif clip.endswith('yuv'):
                        temp_clip = '/dev/shm/{}.yuv'.format(output_filename)
                        '''Generate Encode Commands'''
                        processed_encoding_command = encoding_command_template.format(**vars())
                        '''Generate Metric Command'''
                        processed_metric_command = metric_command_template.format(**vars())

                    per_preset_encode_commands.append(processed_encoding_command)
                    per_preset_metric_commands.append(processed_metric_command)

        processed_encode_commands.append(per_preset_encode_commands)
        processed_metric_commands.append(per_preset_metric_commands)

    return processed_encode_commands, processed_metric_commands, copy_commands, downscale_commands


def get_downscaled_clip_list(clip_lists, downscale_target_resolutions, downscale_template):
    downscaled_clip_map = dict()
    parameter_tracker = dict()
    downscaled_clip_list = list()
    copy_commands = list()
    downscale_commands = list()
    rawvideo = 'rawvideo'
    resolution_id = list(RESOLUTION.keys())[list(RESOLUTION.values()).index(downscale_target_resolutions)]

    for target_resolution in downscale_target_resolutions:
        downscaled_clip_map["{}x{}".format(target_resolution[0], target_resolution[1])] = []   
        
    for i in range(len(clip_lists)):
        for clip in clip_lists[i]:
##            print('clip',clip)
            if (not clip.endswith('yuv') and not clip.endswith('y4m')) or (clip.endswith('yuv') and not yuv_library_found):
                continue
            elif clip.endswith('yuv') and yuv_library_found:
                width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format = get_YUV_PARAMS(clip)
            elif clip.endswith('y4m'):
                width, height, framerate,number_of_frames = read_y4m_header(clip)
            if "{width}x{height}".format(**vars()) not in downscaled_clip_map:
                downscaled_clip_map["{width}x{height}".format(**vars())] = []                 
            if clip.endswith('yuv'):
                if bitdepth == 10:
                    pixfmt = "yuv420p10le"
                else:
                    pixfmt = "yuv420p"

            if 'netflix' in resolution_id:
                if height == 2160:
                    downscale_target_resolutions = [(2560,1440),(1920,1080),(1280,720),(960,540),(768,432),(608,342),(480,270),(384,216)] #8bit
                else:
                    downscale_target_resolutions = [(2560,1088),(1920,816),(1280,544),(960,408),(748,318),(588,250),(480,204),(372,158)] #8bit
                    
            clip_path = clip     
            clip = os.path.join('resized_clips',os.path.split(clip)[-1])
            mod_downscale_template = downscale_template
            match = re.search(r"_\d+x\d+_?",clip)

            if match is None:
                match = re.search(r"_\d+p.*?_",clip,re.IGNORECASE)
                if match is None:
                    match_start = len(clip)-4
                    match_end = len(clip)-4 
            if match is not None:
                match_start = match.start()
                match_end = match.end()

            '''remove yuv tokens in case of y4m clip'''
            if clip.endswith('y4m'):
                downscale_param_tokens = re.findall('\s*\S*\s*{.*?}',downscale_template)
                
                for downscale_param_token in downscale_param_tokens:
                    downscale_param = downscale_param_token.split('{')[-1].split('}')[0]
                    if downscale_param in YUV_PARAMS:
                        mod_downscale_template = mod_downscale_template.replace(downscale_param_token,'')

            clip_beginning = clip[:match_start]
            clip_end = clip[match_end:]                

            ref_clip = "{clip_beginning}_{width}x{height}_{clip_end}".format(**vars())  
            downscaled_clip_map["{width}x{height}".format(**vars())].append(ref_clip)

            '''Create copy command'''
            copy_command = "cp {clip_path} {ref_clip}".format(**vars())
            copy_commands.append(copy_command)

            for target_width,target_height in downscale_target_resolutions:
                if int(target_width) >= int(width) and int(target_height) >= int(height):
                    continue

                new_clip_name = "{clip_beginning}_{width}x{height}to{target_width}x{target_height}_lanc_{clip_end}".format(**vars())
                downscaled_clip_map["{target_width}x{target_height}".format(**vars())].append(new_clip_name)
                fps = '30000/1001'
                target_path = new_clip_name

                '''Fill in downscale template'''
                if clip.endswith('yuv'):
                    downscale_command = downscale_template.format(**vars())
                else:
                    downscale_command = mod_downscale_template.format(**vars())

                '''Keep track of parameters of downscaled clips'''
                if clip.endswith('yuv'):
                    parameter_tracker[new_clip_name]=[width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format]
                    parameter_tracker[ref_clip]=[width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, fps_ratio, fps_decimal, vvenc_pixel_format, pixel_format]
                if clip.endswith('y4m'):
                    parameter_tracker[new_clip_name]=[width, height, framerate,number_of_frames]
                    parameter_tracker[ref_clip]=[width, height, framerate,number_of_frames]
                    
                downscale_commands.append(downscale_command)
                
    key_list=sorted(downscaled_clip_map.keys(),key=lambda k: int(k.split("x")[0]) * int(k.split("x")[1]), reverse=True)
    
    for target_res in key_list:
        downscaled_clip_list.append(downscaled_clip_map[target_res])

    return copy_commands, downscale_commands, downscaled_clip_list, parameter_tracker


def generate_bash_driver_file(encoding_command_template,encode_file_id, metric_file_id, metric_id, resolutions, encode_optimal_job_count, metric_optimal_job_count,ffmpeg_optimal_job_count):
    encoder_exec = ""

    if 'SvtAv1EncApp' in encoding_command_template:
        encoder_exec = "SvtAv1EncApp"
    elif 'aomenc' in encoding_command_template:
        encoder_exec = "aomenc"
    elif 'vpxenc' in encoding_command_template:
        encoder_exec = "vpxenc"
    elif 'vvencapp' in encoding_command_template:
        encoder_exec = "vvencapp"
    elif 'ffmpeg' in encoding_command_template:
        encoder_exec = "ffmpeg"
    elif 'x264' in encoding_command_template:
        encoder_exec = "x264"
    elif 'x265' in encoding_command_template:
        encoder_exec = "x265"
    else:
        print("Warning encoder executable not specified in bash script")

    encoder_test_identifier = encoder_exec
        
    run_all_paral_file_name = 'run-{}-all-paral.sh'.format(encoder_test_identifier)
    run_paral_cpu_file_name = 'run-{}-paral-cpu.sh'.format(encoder_test_identifier)                              

    with open(run_paral_cpu_file_name, 'w') as file:
        file.write("parallel -j {} < {}-m$1.txt".format(encode_optimal_job_count, encode_file_id))

    run_all_paral_script = []
    run_all_paral_script.append("#!/bin/bash")
    run_all_paral_script.append("encoder_type=\"{}\"".format(encoder_exec))
    run_all_paral_script.append("enc_mode_array=({})".format(' '.join(map(str,TEST_SETTINGS['presets']))))
    run_all_paral_script.append("debug_date=" + r"`date " + "\"+%Y-%m-%d_%H%M%S\"`")
    run_all_paral_script.append("debug_filename=" + "\"debug_output_automation_${debug_date}.txt\"")

    run_all_paral_script.append("mkdir {}".format('bitstreams'))

    run_all_paral_script.append("chmod +rx tools")
    run_all_paral_script.append("chmod +rx tools/*")
    run_all_paral_script.append("chmod +rx *.sh")
    run_all_paral_script.append("chmod +x {}".format(encoder_exec))

    if encoder_exec == 'vvencapp':
        run_all_paral_script.append("chmod +x vvdecapp")
    if 'vmaf_exe' in metric_id:
        run_all_paral_script.append('mkdir /dev/shm/bitstreams')
  
##    if encoder == 'ffmpeg-libaom' and passes==2:
##        run_all_paral_script.append("mkdir 2pass_logs")

    if resolutions:
        run_all_paral_script.append("mkdir resized_clips")
        run_all_paral_script.append("parallel -j {} < run_copy_reference.txt".format(ffmpeg_optimal_job_count))
        run_all_paral_script.append("(/usr/bin/time --verbose parallel -j {} < run_downscale.txt) &> time_downscale.log &".format(ffmpeg_optimal_job_count))
        run_all_paral_script.append("wait && echo \'Downscaling Finished\'")

    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")
    run_all_paral_script.append("\tif [ \"$encoder_type\" == \"aomenc\" ] || [ \"$encoder_type\" == \"vpxenc\" ]; then")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) > time_enc_$i.log 2>&1 &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\telse")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) &> time_enc_$i.log &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\tfi\n")
    run_all_paral_script.append("\twait $b && echo \'Encoding Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    run_all_paral_script.append("done")
    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")

    if 'vmaf_exe' in metric_id:
        run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < {}-m$i.txt) &> time_vmaf_extraction_$i.log &".format(metric_optimal_job_count, metric_file_id))
        run_all_paral_script.append("\twait $b && echo \'Running Vmaf exe Extraction for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    else:
        run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < {}-m$i.txt) &> time_vmaf_extraction_$i.log &".format(metric_optimal_job_count, metric_file_id))
        run_all_paral_script.append("\twait $b && echo \'Running metric commands for Encode Mode \'$i\' Finished\' >> ${debug_filename}")

    run_all_paral_script.append("done")
    run_all_paral_script.append("python collect_raw_bdr_speed_data.py")

    with open(run_all_paral_file_name, 'w') as file:
        for line in run_all_paral_script:
            file.write(line + '\n')

    ## chmoding bash files
    # NOTE: os.chmod is finicky -> may not work
    file_stat = os.stat(run_all_paral_file_name)
    os.chmod(run_all_paral_file_name, file_stat.st_mode | stat.S_IEXEC)

    file_stat = os.stat(run_paral_cpu_file_name)
    os.chmod(run_paral_cpu_file_name, file_stat.st_mode | stat.S_IEXEC)

    
'''Two functions to extract clip info from its y4m header'''
def read_y4m_header_helper(readByte, buffer):
    if sys.version_info[0] == 3:
        if (readByte == b'\n' or readByte == b' '):
            clipparameter = buffer
            buffer = b""
            return clipparameter, buffer
        else:
            buffer += readByte
            return -1, buffer
    else:
        if (readByte == '\n' or readByte==' '):
            clipparameter = buffer
            buffer = ""
            return clipparameter, buffer
        else:
            buffer += readByte
            return -1, buffer

        
def read_y4m_header(clip):
    if sys.version_info[0] == 3:
        header_delimiters = {b"W":'width',b"H":'height',b"F":'frame_ratio',b"I":'interlacing',b"A":'pixel_aspect_ratio',b"C":'bitdepth'}
    else:
        header_delimiters = {"W":'width',"H":'height',"F":'frame_ratio',"I":'interlacing',"A":'pixel_aspect_ratio',"C":'bitdepth'}
        
    y4m_params = {'width':            -1,
                  'height':           -1,
                  'frame_ratio':      -1,
                  'framerate':        -1,
                  'number_of_frames':  1,
                  'bitdepth':        -1
                    }
    
    with open(clip, "rb") as f:
        f.seek(10)
        
        if sys.version_info[0] == 3:
            buffer = b""
        else:
            buffer = ""

        while True:
            readByte = f.read(1)
            if (readByte in header_delimiters.keys()):
                y4m_key = readByte
                while 1:
                    readByte = f.read(1)
                    y4m_params[header_delimiters[y4m_key]], buffer = read_y4m_header_helper(readByte, buffer)
                    if y4m_params[header_delimiters[y4m_key]] != -1:
                        break  
          
            if sys.version_info[0] == 3:
                if binascii.hexlify(readByte) == b'0a':
                    break
            else:
                if binascii.hexlify(readByte) == '0a':
                    break
  
        if sys.version_info[0] == 3:
            frame_ratio_pieces = y4m_params['frame_ratio'].split(b":")
            if b'10' in y4m_params['bitdepth']:
                frame_length = int(float(2)*float(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2))
                y4m_params['bitdepth'] = '10bit'
            else:
                frame_length = int(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2)
                y4m_params['bitdepth'] = '8bit'
        else:
            frame_ratio_pieces = y4m_params['frame_ratio'].split(":")
            if '10' in y4m_params['bitdepth']:
                frame_length = int(float(2)*float(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2))
                y4m_params['bitdepth'] = '10bit'
            else:
                frame_length = int(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2)
                y4m_params['bitdepth'] = '8bit'

        y4m_params['framerate'] = float(frame_ratio_pieces[0]) / float(frame_ratio_pieces[1])
        
        while f.tell() < os.path.getsize(clip):
            readByte = f.read(1)
            if binascii.hexlify(readByte) == b'0a':
                f.seek(frame_length,1)
                buff = binascii.hexlify(f.read(5))
                if buff == b'4652414d45':
                    y4m_params['number_of_frames'] += 1
                 
    return int(y4m_params['width']), int(y4m_params['height']), y4m_params['framerate'], y4m_params['number_of_frames']


'''functions to get clip info'''
def get_fps(clipdir, clip):
    if(".yuv" in clip and yuv_library_found):
        seq_table_index = get_seq_table_loc(seq_list, clip)
        if seq_table_index < 0:
            return 0
        fps = float(seq_list[seq_table_index]["fps_num"]) / seq_list[seq_table_index]["fps_denom"]
        return fps
    elif(".y4m" in clip):
        _, _, framerate,number_of_frames = read_y4m_header(os.path.join(clipdir, clip))
        return framerate
    else:
        return 0

    
def get_seq_table_loc(seq_table, clip_name):
    for i in range(len(seq_table)):
        if seq_table[i]["name"] == clip_name[:-4]:
            return i
    return -1


def get_YUV_PARAMS(clip):
    clip_name = os.path.split(clip)[1]
    seq_table_index = get_seq_table_loc(seq_list, clip_name)

    bitdepth = seq_list[seq_table_index]['bitdepth']
    ten_bit_format = seq_list[seq_table_index]['unpacked']
    width = seq_list[seq_table_index]['width']
    height = seq_list[seq_table_index]['height']
    width_x_height = '%sx%s' % (width,height)
    fps_num = seq_list[seq_table_index]['fps_num']
    fps_denom = seq_list[seq_table_index]['fps_denom']
    
    if (bitdepth == 8):
        number_of_frames = (int)(os.path.getsize(clip)/(width*height+(width*height/2)))
        pixel_format='yuv420p'
        vvenc_pixel_format='yuv420'
    elif (bitdepth == 10):
        pixel_format='yuv420p10le'
        vvenc_pixel_format='yuv42010'#Not sure what the real one is
        if ten_bit_format == 2:
            number_of_frames = (int)(((float)(os.path.getsize(clip))/(width*height+(width*height/2)))/1.25)
        else:
            number_of_frames = (int)(((os.path.getsize(clip))/(width*height+(width*height/2)))/2)
            
    return width, height, width_x_height, fps_num, fps_denom, bitdepth, number_of_frames, '%s/%s'%(fps_num,fps_denom), float(float(fps_num)/float(fps_denom)),vvenc_pixel_format, pixel_format


def sort_clip_list_by_complexity(input_folder):
    files = glob.glob('%s/*'%input_folder)
    files.sort(key=lambda f: get_fps(input_folder, f), reverse=True)
    files.sort(key=lambda f: f.lower())
    files.sort(key=lambda f: (os.stat(os.path.join(input_folder, f)).st_size), reverse=True)
    Y4M_HEADER_SIZE = 80

    ## sort all files in lists by size
    clip_lists = []
    clip_list = []
    size_0 = os.stat(os.path.join(input_folder, files[0])).st_size
    for file_ in files:
        if (file_.endswith('yuv') or file_.endswith('y4m')):
            if file_.endswith('yuv') and os.stat(os.path.join(input_folder, file_)).st_size == size_0:
                clip_list.append(file_)
            elif file_.endswith('y4m') and size_0-Y4M_HEADER_SIZE <= os.stat(os.path.join(input_folder, file_)).st_size <= size_0+Y4M_HEADER_SIZE:
                clip_list.append(file_)
            else:
                clip_lists.append(clip_list)
                clip_list = []
                size_0 = os.stat(os.path.join(input_folder, file_)).st_size 
                clip_list.append(file_)
    clip_lists.append(clip_list)
    
    return clip_lists


def execute_command_helper(inputs):
    cmd, work_dir = inputs
    print(cmd)
    pipe = subprocess.Popen(cmd, shell=True, cwd=work_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, error = pipe.communicate()
    pipe.wait()
    memory_use = re.search(r'Maximum resident set size \(kbytes\):\s(.*?)\n',error).group(1)
    print('memory_use',memory_use)
    # print(error)
    return memory_use


def execute_parallel_commands(number_of_processes, command_list, execution_directory):
    command_lines = [command.strip() for command in command_list]
    execution_directory_list = [execution_directory for i in enumerate(command_lines)]
    inputs = zip(command_lines, execution_directory_list)
    
    Pooler = multiprocessing.Pool(processes=number_of_processes, maxtasksperchild=30)

    mem_results = Pooler.map(execute_command_helper, inputs)
    Pooler.close()
    Pooler.join()
    
    if sys.platform == 'Linux':
        os.system('stty sane')

    return mem_results


try:
    from yuv_library import getyuvlist
    seq_list = getyuvlist()
    yuv_library_found = 1
except ImportError:
    print("WARNING yuv_library not found, only generating commands for y4m files.")
    seq_list = []
    yuv_library_found = 0

WORKING_DIRECTORY = os.getcwd()



if __name__ == '__main__':
    main()
